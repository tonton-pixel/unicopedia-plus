//
module.exports.create = function (kangxiRadicals)
{
    const unihan = require ('../../lib/unicode/unihan.js');
    //
    const { fromRadicalStrokes } = require ('../../lib/unicode/get-rs-strings.js');
    const { getCharacterBasicData } = require ('../../lib/unicode/unicode.js');
    //
    function getTooltip (radical)
    {
        let data = getCharacterBasicData (radical);
        // return `${data.codePoint.replace (/U\+/, "U\u034F\+")}\xA0${data.name}`; // U+034F COMBINING GRAPHEME JOINER
        return `${data.codePoint.replace (/U\+/, "U\u034F\+")}\xA0${radical}\xA0${data.name}`; // U+034F COMBINING GRAPHEME JOINER
    }
    //
    const orderedCategories =
    [
        "Variant Forms",
        "Simplified",
        "C-Simplified",
        "J-Simplified",
        "V-Simplified"
    ];
    let categories = { };
    for (let radical of kangxiRadicals)
    {
        if ("cjk" in radical)
        {
            for (let cjk of radical.cjk)
            {
                if ("category" in cjk)
                {
                    let category = cjk.category;
                    if (category in categories)
                    {
                        categories[category]++;
                    }
                    else
                    {
                        categories[category] = 1;
                    }
                }
            }
        }
    }
    //
    let radicalsTable = document.createElement ('table');
    radicalsTable.className = 'radicals-table';
    let headerRow = document.createElement ('tr');
    headerRow.className = 'header-row';
    let headerIndex = document.createElement ('th');
    headerIndex.className = 'header-index';
    headerIndex.textContent = "#";
    headerRow.appendChild (headerIndex);
    let headerRadical = document.createElement ('th');
    headerRadical.className = 'header-radical';
    headerRadical.textContent = "Rad.";
    headerRow.appendChild (headerRadical);
    let headerName = document.createElement ('th');
    headerName.className = 'header-name';
    headerName.textContent = "Name";
    headerRow.appendChild (headerName);
    for (let category of orderedCategories)
    {
        if (category in categories)
        {
            let variantHeader = document.createElement ('th');
            variantHeader.className = 'header-variants';
            variantHeader.textContent = category;
            headerRow.appendChild (variantHeader);
        }
    }
    radicalsTable.appendChild (headerRow);
    let lastStrokes = 0;
    let evenOdd;
    let columns = 1 + // index
                  1 + // radical
                  1 + // name
                  Object.keys (categories).length;
    for (let radical of kangxiRadicals)
    {
        if (lastStrokes !== radical.strokes)
        {
            let strokesRow = document.createElement ('tr');
            strokesRow.className = 'strokes-row';
            let dataStrokes = document.createElement ('td');
            dataStrokes.className = 'data-strokes';
            dataStrokes.colSpan = columns;
            dataStrokes.textContent = fromRadicalStrokes (radical.strokes, true);
            strokesRow.appendChild (dataStrokes);
            radicalsTable.appendChild (strokesRow);
            lastStrokes = radical.strokes;
            evenOdd = 0;
        }
        let dataRow = document.createElement ('tr');
        dataRow.className = 'data-row';
        dataRow.classList.add (evenOdd++ % 2 ? 'odd' : 'even');
        let dataIndex = document.createElement ('td');
        dataIndex.className = 'data-index';
        dataIndex.textContent = radical.number;
        dataRow.appendChild (dataIndex);
        let dataRadical = document.createElement ('td');
        dataRadical.className = 'data-radical';
        dataRadical.textContent = radical.radical;
        dataRadical.title = getTooltip (radical.radical);
        dataRow.appendChild (dataRadical);
        let dataName = document.createElement ('td');
        dataName.className = 'data-name';
        dataName.textContent = radical.name;
        dataRow.appendChild (dataName);
        for (let category of orderedCategories)
        {
            if (category in categories)
            {
                let dataVariants = document.createElement ('td');
                dataVariants.className = 'data-variants';
                if ("cjk" in radical)
                {
                    for (let cjk of radical["cjk"])
                    {
                        if (cjk.category === category)
                        {
                            let dataVariant = document.createElement ('span');
                            dataVariant.className = 'data-variant';
                            let variant = cjk.radical || cjk.unified;
                            if (!unihan.isRadical (variant))
                            {
                                dataVariant.classList.add ('unified');
                            }
                            dataVariant.textContent = variant;
                            dataVariant.title = getTooltip (variant);
                            dataVariants.appendChild (dataVariant);
                        }
                    }
                }
                dataRow.appendChild (dataVariants);
            }
        }
        radicalsTable.appendChild (dataRow);
    }
    let footerRow = headerRow.cloneNode (true);
    radicalsTable.appendChild (footerRow);
    return radicalsTable;
}
//
