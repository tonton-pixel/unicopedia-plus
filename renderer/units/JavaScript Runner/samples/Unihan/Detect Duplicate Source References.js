// Detect Duplicate Source References
const { codePoints } = require ('./lib/unicode/parsed-unihan-data.js');
const sourceProperties =
[
    "kIRG_GSource",
    "kIRG_HSource",
    "kIRG_JSource",
    "kIRG_KPSource",
    "kIRG_KSource",
    "kIRG_MSource",
    "kIRG_SSource",
    "kIRG_TSource",
    "kIRG_UKSource",
    "kIRG_USource",
    "kIRG_VSource"
];
let propertyValues = { };
for (let codePoint in codePoints)
{
    for (let sourceProperty of sourceProperties)
    {
        let propertyValue = codePoints[codePoint][sourceProperty];
        if (propertyValue)
        {
            if (propertyValue in propertyValues)
            {
                propertyValues[propertyValue]++;
            }
            else
            {
                propertyValues[propertyValue] = 1;
            }
        }
    }
}
let sortedPropertyValueKeys = Object.keys (propertyValues).sort ();
for (let propertyValue of sortedPropertyValueKeys)
{
    if (propertyValues[propertyValue] > 1)
    {
        $.writeln (`${JSON.stringify (propertyValue)}: ${propertyValues[propertyValue]}`);
    }
}
