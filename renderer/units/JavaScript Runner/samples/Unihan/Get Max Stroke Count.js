// Get Max Stroke Count
const { codePoints } = require ('./lib/unicode/parsed-unihan-data.js');
const extraSources = true;
const rsProperties =
[
    "kRSUnicode",
    "kRSAdobe_Japan1_6"
];
//
let maxStrokes = -1;
let maxCodePoint = "";
let maxPropertyValue = "";
for (let codePoint in codePoints)
{
    for (let rsProperty of rsProperties)
    {
        if (extraSources || (rsProperty === "kRSUnicode"))
        {
            let rsPropertyValues = codePoints[codePoint][rsProperty];
            if (rsPropertyValues)
            {
                let rsValue;
                if (!Array.isArray (rsPropertyValues))
                {
                    rsPropertyValues = [ rsPropertyValues ];
                }
                for (let rsPropertyValue of rsPropertyValues)
                {
                    if (rsProperty === "kRSAdobe_Japan1_6")
                    {
                        let parsed = rsPropertyValue.match (/^[CV]\+[0-9]{1,5}\+([1-9][0-9]{0,2}\.[1-9][0-9]?\.[0-9]{1,2})$/);
                        if (parsed)
                        {
                            let [ index, strokes, residual ] = parsed[1].split (".");
                            rsValue = [ index, residual ].join (".");
                        }
                    }
                    else
                    {
                        rsValue = rsPropertyValue;
                    }
                    let [ index, residual ] = rsValue.split (".");
                    let residualStrokes = Math.max (parseInt (residual), 0);
                    if (residualStrokes > maxStrokes)
                    {
                        maxStrokes = residualStrokes;
                        maxCodePoint = codePoint;
                        maxPropertyValue = `${rsProperty} ${rsValue}`;
                    }
                }
            }
        }
    }
}
$.writeln (`Max Strokes: ${maxStrokes}`);
$.writeln (`Character: ${String.fromCodePoint (parseInt (maxCodePoint.replace ("U+", ""), 16))}`);
$.writeln (`Code Point: ${maxCodePoint}`);
$.writeln (`Property Value: ${maxPropertyValue}`);
