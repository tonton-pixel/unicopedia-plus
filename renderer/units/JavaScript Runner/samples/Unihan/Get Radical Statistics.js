// Get Radical Statistics
const { codePoints, coreSet, core2020Set, fullSet } = require ('./lib/unicode/parsed-unihan-data.js');
const { fromRadical } = require ('./lib/unicode/get-rs-strings.js');
const sets = [ fullSet, core2020Set, coreSet ];
const extraSources = false;
const rsProperties =
[
    "kRSUnicode",
    "kRSAdobe_Japan1_6"
];
//
for (let set of sets)
{
    const radicals = [ ];
    for (let index = 0; index < 214; index++)
    {
        radicals.push ({ index: index + 1, count: 0 });
    }
    for (let codePoint of set)
    {
        for (let rsProperty of rsProperties)
        {
            if (extraSources || (rsProperty === "kRSUnicode"))
            {
                let rsPropertyValues = codePoints[codePoint][rsProperty];
                if (rsPropertyValues)
                {
                    let rsValue;
                    if (!Array.isArray (rsPropertyValues))
                    {
                        rsPropertyValues = [ rsPropertyValues ];
                    }
                    for (let rsPropertyValue of rsPropertyValues)
                    {
                        if (rsProperty === "kRSAdobe_Japan1_6")
                        {
                            let parsed = rsPropertyValue.match (/^[CV]\+[0-9]{1,5}\+([1-9][0-9]{0,2}\.[1-9][0-9]?\.[0-9]{1,2})$/);
                            if (parsed)
                            {
                                let [ index, strokes, residual ] = parsed[1].split (".");
                                rsValue = [ index, residual ].join (".");
                            }
                        }
                        else
                        {
                            rsValue = rsPropertyValue;
                        }
                        let [ index, residual ] = rsValue.split (".");
                        radicals[parseInt (index) - 1].count++;
                    }
                }
            }
        }
    }
    radicals.sort ((a, b) => a.count - b.count).reverse ();
    let setName;
    switch (set)
    {
        case coreSet:
            setName = "IICore";
            break;
        case core2020Set:
            setName = "Unihan Core";
            break;
        case fullSet:
            setName = "Full Unihan";
            break;
    }
    $.writeln (`• ${setName} set${extraSources ? " + Extra R/S Sources" : ""}:`);
    for (let index = 0; index < radicals.length; index++)
    {
        $.writeln (`${fromRadical (radicals[index].index, "", true)}: ${radicals[index].count} characters`);
    }
    $.writeln ("------------------------------------------------");
    $.writeln ();
}
