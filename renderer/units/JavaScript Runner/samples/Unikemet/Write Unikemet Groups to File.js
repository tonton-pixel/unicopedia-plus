// Write Unikemet Groups to File
const { groups } = require ('./lib/unicode/parsed-unikemet-data.js');
let start = window.performance.now ();
let jsonFile = $.save ($.stringify (groups, null, 4), 'unikemet-groups.json');
let stop = window.performance.now ();
$.writeln (`Wrote Unikemet groups to JSON file in ${((stop - start) / 1000).toFixed (2)} seconds:\n${jsonFile}`);
