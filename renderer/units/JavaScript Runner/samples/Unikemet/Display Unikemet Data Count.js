// Display Unikemet Data Count
const { coreSet, fullSet } = require ('./lib/unicode/parsed-unikemet-data.js');
$.writeln (`Core Unikemet: ${coreSet.length.toLocaleString ('en')}`);
$.writeln (`Full Unikemet: ${fullSet.length.toLocaleString ('en')}`);
