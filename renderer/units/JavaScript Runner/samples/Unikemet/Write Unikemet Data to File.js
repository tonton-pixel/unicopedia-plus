// Write Unikemet Data to File
const { codePoints, fullSet } = require ('./lib/unicode/parsed-unikemet-data.js');
let start = window.performance.now ();
let sortedCodePoints = { };
fullSet.forEach (codePoint => { sortedCodePoints[codePoint] = codePoints[codePoint]; });
let jsonFile = $.save ($.stringify (sortedCodePoints, null, 4), 'unikemet-data.json');
let stop = window.performance.now ();
$.writeln (`Wrote Unikemet data to JSON file in ${((stop - start) / 1000).toFixed (2)} seconds:\n${jsonFile}`);
