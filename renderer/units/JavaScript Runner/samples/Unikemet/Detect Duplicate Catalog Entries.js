// Detect Duplicate Catalog Entries
const { codePoints } = require ('./lib/unicode/parsed-unikemet-data.js');
let catalogValues = { };
for (let codePoint in codePoints)
{
    let catalogValue = codePoints[codePoint]["kEH_Cat"];
    if (catalogValue)
    {
        if (catalogValue in catalogValues)
        {
            catalogValues[catalogValue]++;
        }
        else
        {
            catalogValues[catalogValue] = 1;
        }
    }
    else
    {
        $.writeln (`Missing catalog entry for: ${codePoint}`);
    }
}
let sortedCatalogValueKeys = Object.keys (catalogValues).sort ();
for (let catalogValue of sortedCatalogValueKeys)
{
    if (catalogValues[catalogValue] > 1)
    {
        $.writeln (`${JSON.stringify (catalogValue)}: ${catalogValues[catalogValue]}`);
    }
}
