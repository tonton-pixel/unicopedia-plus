//
module.exports.create = function (unikemetBlocks)
{
    let blocksTable = document.createElement ('table');
    blocksTable.className = 'blocks-table';
    let headerRow = document.createElement ('tr');
    headerRow.className = 'header-row';
    let blockHeader = document.createElement ('th');
    blockHeader.className = 'block-header';
    blockHeader.textContent = "Block";
    headerRow.appendChild (blockHeader);
    let rangeHeader = document.createElement ('th');
    rangeHeader.className = 'range-header';
    rangeHeader.textContent = "Range";
    headerRow.appendChild (rangeHeader);
    let sizeHeader = document.createElement ('th');
    sizeHeader.className = 'size-header';
    sizeHeader.textContent = "Size";
    headerRow.appendChild (sizeHeader);
    let coreSetHeader = document.createElement ('th');
    coreSetHeader.className = 'core-set-header';
    coreSetHeader.textContent = "Core\xA0Set";
    coreSetHeader.title = "Core Unikemet";
    headerRow.appendChild (coreSetHeader);
    let fullSetHeader = document.createElement ('th');
    fullSetHeader.className = 'full-set-header';
    fullSetHeader.textContent = "Full\xA0Set";
    fullSetHeader.title = "Full Unikemet";
    headerRow.appendChild (fullSetHeader);
    blocksTable.appendChild (headerRow);
    let coreTotal = 0;
    let fullTotal = 0;
    for (let unikemetBlock of unikemetBlocks)
    {
        let dataRow = document.createElement ('tr');
        dataRow.className = 'data-row';
        let dataName = document.createElement ('td');
        dataName.className = 'name';
        dataName.textContent = unikemetBlock.name.replace (/ (.)$/, "\u00A0$1");
        dataRow.appendChild (dataName);
        let dataRange = document.createElement ('td');
        dataRange.className = 'range';
        dataRange.textContent = `U+${unikemetBlock.first}..U+${unikemetBlock.last}`;
        dataRow.appendChild (dataRange);
        let dataSize = document.createElement ('td');
        dataSize.className = 'size';
        dataSize.textContent = unikemetBlock.size;
        dataSize.title = `${unikemetBlock.size / 16} × 16`;
        dataRow.appendChild (dataSize);
        let dataCoreSet = document.createElement ('td');
        dataCoreSet.className = 'core-set';
        let coreCount = unikemetBlock.coreCount;
        coreTotal += coreCount;
        dataCoreSet.textContent = coreCount.toLocaleString ('en');
        dataRow.appendChild (dataCoreSet);
        let dataFullSet = document.createElement ('td');
        dataFullSet.className = 'full-set';
        let fullCount = unikemetBlock.fullCount;
        fullTotal += fullCount;
        dataFullSet.textContent = fullCount.toLocaleString ('en');
        dataRow.appendChild (dataFullSet);
        blocksTable.appendChild (dataRow);
    }
    let totalRow = document.createElement ('tr');
    totalRow.className = 'data-row';
    let dataPaddingTotal = document.createElement ('td');
    dataPaddingTotal.colSpan = 3;
    dataPaddingTotal.className = 'padding-total';
    dataPaddingTotal.textContent = "";
    totalRow.appendChild (dataPaddingTotal);
    let dataCoreTotal = document.createElement ('td');
    dataCoreTotal.className = 'core-total';
    dataCoreTotal.textContent = coreTotal.toLocaleString ('en');
    totalRow.appendChild (dataCoreTotal);
    let dataFullTotal = document.createElement ('td');
    dataFullTotal.className = 'full-total';
    dataFullTotal.textContent = fullTotal.toLocaleString ('en');
    totalRow.appendChild (dataFullTotal);
    blocksTable.appendChild (totalRow);
    return blocksTable;
}
//
