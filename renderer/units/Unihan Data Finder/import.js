//
const unit = document.getElementById ('unihan-data-finder-unit');
//
const tabs = unit.querySelectorAll ('.tab-bar .tab-radio');
const tabPanes = unit.querySelectorAll ('.tab-panes .tab-pane');
const tabInfos = unit.querySelectorAll ('.tab-infos .tab-info');
//
const propertySelect = unit.querySelector ('.find-by-property .property-select');
const propertyCategoriesCheckbox = unit.querySelector ('.find-by-property .categories-checkbox');
const propertySearchString = unit.querySelector ('.find-by-property .search-string');
const propertySearchMessage = unit.querySelector ('.find-by-property .search-message');
const propertyWholeWord = unit.querySelector ('.find-by-property .whole-word');
const propertyUseRegex = unit.querySelector ('.find-by-property .use-regex');
const propertySearchButton = unit.querySelector ('.find-by-property .search-button');
const propertyResultsButton = unit.querySelector ('.find-by-property .results-button');
const propertyHitCount = unit.querySelector ('.find-by-property .hit-count');
const propertyTotalCount = unit.querySelector ('.find-by-property .total-count');
const propertySearchData = unit.querySelector ('.find-by-property .search-data');
const propertyInstructions = unit.querySelector ('.find-by-property .instructions');
const propertyRegexExamples = unit.querySelector ('.find-by-property .regex-examples');
const propertyReferences = unit.querySelector ('.find-by-property .references');
const propertyLinks = unit.querySelector ('.find-by-property .links');
//
const propertyParams = { };
//
let propertyCurrentProperty;
let propertyShowCategories;
//
const matchSearchString = unit.querySelector ('.match-character .search-string');
const matchSearchMessage = unit.querySelector ('.match-character .search-message');
const matchVariants = unit.querySelector ('.match-character .match-variants');
const matchUseRegex = unit.querySelector ('.match-character .use-regex');
const matchSearchButton = unit.querySelector ('.match-character .search-button');
const matchResultsButton = unit.querySelector ('.match-character .results-button');
const matchHitCount = unit.querySelector ('.match-character .hit-count');
const matchTotalCount = unit.querySelector ('.match-character .total-count');
const matchSearchData = unit.querySelector ('.match-character .search-data');
const matchInstructions = unit.querySelector ('.match-character .instructions');
const matchRegexExamples = unit.querySelector ('.match-character .regex-examples');
const matchReferences = unit.querySelector ('.match-character .references');
const matchLinks = unit.querySelector ('.match-character .links');
//
const matchParams = { };
//
const gridSpecimenHistoryButton = unit.querySelector ('.view-by-grid .history-button');
const gridSpecimen = unit.querySelector ('.view-by-grid .specimen');
const gridGoButton = unit.querySelector ('.view-by-grid .go-button');
const gridSelectBlockRange = unit.querySelector ('.view-by-grid .select-block-range');
const gridSelectBlockName = unit.querySelector ('.view-by-grid .select-block-name');
const gridResultsButton = unit.querySelector ('.view-by-grid .results-button');
const gridHitCount = unit.querySelector ('.view-by-grid .hit-count');
const gridTotalCount = unit.querySelector ('.view-by-grid .total-count');
const gridSearchData = unit.querySelector ('.view-by-grid .search-data');
const gridInstructions = unit.querySelector ('.view-by-grid .instructions');
const gridUnihanBlocks = unit.querySelector ('.view-by-grid .unihan-blocks');
const gridBlocks = unit.querySelector ('.view-by-grid .blocks');
const gridReferences = unit.querySelector ('.view-by-grid .references');
const gridLinks = unit.querySelector ('.view-by-grid .links');
//
const gridParams = { };
//
const gridSpecimenHistorySize = 128;   // 0: unlimited
//
let gridSpecimenHistory = [ ];
let gridSpecimenHistoryIndex = -1;
let gridSpecimenHistorySave = null;
//
let defaultFolderPath;
//
module.exports.start = function (context)
{
    const { clipboard, shell } = require ('electron');
    const { Menu } = require ('@electron/remote');
    //
    const path = require ('path');
    //
    const fileDialogs = require ('../../lib/file-dialogs.js');
    const pullDownMenus = require ('../../lib/pull-down-menus.js');
    const linksList = require ('../../lib/links-list.js');
    //
    const regexp = require ('../../lib/unicode/regexp.js');
    const unicode = require ('../../lib/unicode/unicode.js');
    const unihan = require ('../../lib/unicode/unihan.js');
    const unihanData = require ('../../lib/unicode/parsed-unihan-data.js');
    //
    let unihanCount = unihanData.fullSet.length;
    //
    const defaultPrefs =
    {
        tabName: "",
        //
        propertySelect: "",
        propertyShowCategories: false,
        propertySearchString: "",
        propertyWholeWord: false,
        propertyUseRegex: false,
        propertyPageSize: 8,
        propertyInstructions: true,
        propertyRegexExamples: false,
        propertyReferences: false,
        //
        matchSearchString: "",
        matchVariants: false,
        matchUseRegex: false,
        matchPageSize: 8,
        matchInstructions: true,
        matchRegexExamples: false,
        matchReferences: false,
        //
        gridSelectBlockRange: "4E00-9FFF",  // CJK Unified Ideographs
        gridSpecimenHistory: [ ],
        gridPageSize: 128,
        gridPageIndex: 0,
        gridInstructions: true,
        gridUnihanBlocks: false,
        gridReferences: false,
        //
        defaultFolderPath: context.defaultFolderPath
    };
    let prefs = context.getPrefs (defaultPrefs);
    //
    defaultFolderPath = prefs.defaultFolderPath;
    //
    function saveResults (string)
    {
        fileDialogs.saveTextFile
        (
            "Save text file:",
            [ { name: "Text (*.txt)", extensions: [ 'txt' ] } ],
            defaultFolderPath,
            (filePath) =>
            {
                defaultFolderPath = path.dirname (filePath);
                return string;
            }
        );
    }
    //
    function updateTab (tabName)
    {
        let foundIndex = 0;
        tabs.forEach
        (
            (tab, index) =>
            {
                let match = (tab.parentElement.textContent === tabName);
                if (match)
                {
                    foundIndex = index;
                }
                else
                {
                    tab.checked = false;
                    tabPanes[index].hidden = true;
                    tabInfos[index].hidden = true;
                }
            }
        );
        tabs[foundIndex].checked = true;
        tabPanes[foundIndex].hidden = false;
        tabInfos[foundIndex].hidden = false;
    }
    //
    updateTab (prefs.tabName);
    //
    for (let tab of tabs)
    {
        tab.addEventListener ('click', (event) => { updateTab (event.currentTarget.parentElement.textContent); });
    }
    //
    function clearSearch (data)
    {
        while (data.firstChild)
        {
            data.firstChild.remove ();
        }
    }
    //
    const propertyDataTable = require ('./property-data-table.js');
    //
    propertyParams.pageSize = prefs.propertyPageSize;
    propertyParams.observer = null;
    propertyParams.root = unit;
    //
    propertyCurrentProperty = prefs.propertySelect;
    propertyShowCategories = prefs.propertyShowCategories;
    //
    function updatePropertyMenu ()
    {
        while (propertySelect.firstChild)
        {
            propertySelect.firstChild.remove ();
        }
        if (propertyShowCategories)
        {
            for (let category of unihanData.categories)
            {
                let optionGroup = document.createElement ('optgroup');
                optionGroup.label = "◎\xA0" + category.name;
                for (let property of category.properties)
                {
                    let option = document.createElement ('option');
                    option.textContent = property;
                    option.title = unihanData.properties[property].name;
                    optionGroup.appendChild (option);
                }
                propertySelect.appendChild (optionGroup);
            }
        }
        else
        {
            let sortedProperties = Object.keys (unihanData.properties).sort ((a, b) => a.localeCompare (b));
            for (let property of sortedProperties)
            {
                let option = document.createElement ('option');
                option.textContent = property;
                option.title = unihanData.properties[property].name;
                propertySelect.appendChild (option);
            }
        }
        //
        propertySelect.value = propertyCurrentProperty;
        if (propertySelect.selectedIndex < 0) // -1: no element is selected
        {
            propertySelect.selectedIndex = 0;
        }
        propertyCurrentProperty = propertySelect.value;
    }
    //
    propertyCategoriesCheckbox.checked = propertyShowCategories;
    propertyCategoriesCheckbox.addEventListener
    (
        'input',
        event =>
        {
            propertyShowCategories = event.currentTarget.checked;
            updatePropertyMenu ();
        }
    );
    //
    propertySelect.addEventListener ('input', event => { propertyCurrentProperty = event.currentTarget.value; });
    updatePropertyMenu ();
    //
    propertyWholeWord.checked = prefs.propertyWholeWord;
    propertyUseRegex.checked = prefs.propertyUseRegex;
    //
    propertySearchString.addEventListener
    (
        'keypress',
        (event) =>
        {
            if (event.key === 'Enter')
            {
                event.preventDefault ();
                propertySearchButton.click ();
            }
        }
    );
    propertySearchString.addEventListener
    (
        'focusin',
        (event) =>
        {
            if (event.currentTarget.classList.contains ('error'))
            {
                propertySearchMessage.classList.add ('shown');
            }
        }
    );
    propertySearchString.addEventListener
    (
        'focusout',
        (event) =>
        {
            if (event.currentTarget.classList.contains ('error'))
            {
                propertySearchMessage.classList.remove ('shown');
            }
        }
    );
    propertySearchString.addEventListener
    (
        'input',
        (event) =>
        {
            event.currentTarget.classList.remove ('error');
            propertySearchMessage.textContent = "";
            propertySearchMessage.classList.remove ('shown');
            if (propertyUseRegex.checked)
            {
                try
                {
                    regexp.build (event.currentTarget.value, { useRegex: propertyUseRegex.checked });
                }
                catch (e)
                {
                    event.currentTarget.classList.add ('error');
                    propertySearchMessage.textContent = e.message;
                    if (event.currentTarget === document.activeElement)
                    {
                        propertySearchMessage.classList.add ('shown');
                    }
                }
            }
        }
    );
    propertySearchString.value = prefs.propertySearchString;
    propertySearchString.dispatchEvent (new Event ('input'));
    //
    propertyUseRegex.addEventListener
    (
        'change',
        (event) => propertySearchString.dispatchEvent (new Event ('input'))
    );
    //
    function findCharactersByProperty (regex, property)
    {
        let characterInfoList = [ ];
        let codePoints = unihanData.codePoints;
        for (let codePoint of unihanData.fullSet)
        {
            if (property in codePoints[codePoint])
            {
                let values = codePoints[codePoint][property];
                if (!Array.isArray (values))
                {
                    values = [ values ];
                }
                let matchingValues = [ ];
                for (let value of values)
                {
                    matchingValues.push (value.match (regex) !== null);
                }
                if (matchingValues.includes (true))
                {
                    let character = String.fromCodePoint (parseInt (codePoint.replace ("U+", ""), 16));
                    characterInfoList.push
                    (
                        {
                            character: character,
                            codePoint: codePoint,
                            property: property,
                            value: ((values.length > 1) ? values : values[0]),
                            matching: ((matchingValues.length > 1) ? matchingValues : matchingValues[0])
                        }
                    );
                }
            }
        }
        return characterInfoList;
    }
    //
    function updatePropertyResults (hitCount, totalCount)
    {
        propertyHitCount.textContent = hitCount;
        propertyTotalCount.textContent = totalCount;
        propertyResultsButton.disabled = (hitCount <= 0);
    }
    //
    let currentCharactersByProperty = [ ];
    //
    propertySearchButton.addEventListener
    (
        'click',
        (event) =>
        {
            if (!propertySearchString.classList.contains ('error'))
            {
                let searchString = propertySearchString.value;
                if (searchString)
                {
                    let regex = null;
                    try
                    {
                        regex = regexp.build (searchString, { wholeWord: propertyWholeWord.checked, useRegex: propertyUseRegex.checked });
                    }
                    catch (e)
                    {
                    }
                    if (regex)
                    {
                        clearSearch (propertySearchData);
                        let characterInfos = findCharactersByProperty (regex, propertyCurrentProperty);
                        currentCharactersByProperty = characterInfos.map (info => info.character);
                        updatePropertyResults (currentCharactersByProperty.length, unihanCount);
                        if (characterInfos.length > 0)
                        {
                            propertySearchData.appendChild (propertyDataTable.create (characterInfos, propertyParams));
                        }
                    }
                }
            }
            else
            {
                shell.beep ();
            }
        }
    );
    //
    let propertyResultsMenu =
    Menu.buildFromTemplate
    (
        [
            {
                label: "Copy Search Results", // "Copy Search Results as String"
                click: () =>
                {
                    if (currentCharactersByProperty.length > 0)
                    {
                        clipboard.writeText (currentCharactersByProperty.join (""));
                    }
                }
            },
            {
                label: "Save Search Results...", // "Save Search Results to File"
                click: () =>
                {
                    saveResults (currentCharactersByProperty.join (""));
                }
            },
            { type: 'separator' },
            {
                label: "Clear Search Results",
                click: () =>
                {
                    clearSearch (propertySearchData);
                    currentCharactersByProperty = [ ];
                    updatePropertyResults (currentCharactersByProperty.length, unihanCount);
                }
            }
        ]
    );
    //
    propertyResultsButton.addEventListener
    (
        'click',
        (event) =>
        {
            pullDownMenus.popup (event.currentTarget, propertyResultsMenu);
        }
    );
    //
    updatePropertyResults (currentCharactersByProperty.length, unihanCount);
    //
    propertyInstructions.open = prefs.propertyInstructions;
    propertyRegexExamples.open = prefs.propertyRegexExamples;
    //
    propertyReferences.open = prefs.propertyReferences;
    //
    const propertyRefLinks = require ('./property-ref-links.json');
    linksList (propertyLinks, propertyRefLinks);
    //
    const matchDataTable = require ('./match-data-table.js');
    //
    const japaneseVariants = require ('../../lib/unicode/parsed-japanese-variants-data.js');
    const yasuokaVariants = require ('../../lib/unicode/parsed-yasuoka-variants-data.js');
    //
    matchParams.pageSize = prefs.matchPageSize;
    matchParams.observer = null;
    matchParams.root = unit;
    //
    matchVariants.checked = prefs.matchVariants;
    //
    matchUseRegex.checked = prefs.matchUseRegex;
    //
    matchSearchString.addEventListener
    (
        'keypress',
        (event) =>
        {
            if (event.key === 'Enter')
            {
                event.preventDefault ();
                matchSearchButton.click ();
            }
        }
    );
    matchSearchString.addEventListener
    (
        'focusin',
        (event) =>
        {
            if (event.currentTarget.classList.contains ('error'))
            {
                matchSearchMessage.classList.add ('shown');
            }
        }
    );
    matchSearchString.addEventListener
    (
        'focusout',
        (event) =>
        {
            if (event.currentTarget.classList.contains ('error'))
            {
                matchSearchMessage.classList.remove ('shown');
            }
        }
    );
    matchSearchString.addEventListener
    (
        'input',
        (event) =>
        {
            event.currentTarget.classList.remove ('error');
            matchSearchMessage.textContent = "";
            matchSearchMessage.classList.remove ('shown');
            if (matchUseRegex.checked)
            {
                try
                {
                    regexp.build (event.currentTarget.value, { useRegex: matchUseRegex.checked });
                }
                catch (e)
                {
                    event.currentTarget.classList.add ('error');
                    matchSearchMessage.textContent = e.message;
                    if (event.currentTarget === document.activeElement)
                    {
                        matchSearchMessage.classList.add ('shown');
                    }
                }
            }
        }
    );
    matchSearchString.value = prefs.matchSearchString;
    matchSearchString.dispatchEvent (new Event ('input'));
    //
    matchUseRegex.addEventListener
    (
        'change',
        (event) => matchSearchString.dispatchEvent (new Event ('input'))
    );
    //
    let variantProperties =
    [
        'kCompatibilityVariant',
        'kSemanticVariant',
        'kSimplifiedVariant',
        'kSpecializedSemanticVariant',
        "kSpoofingVariant",
        'kTraditionalVariant',
        'kZVariant'
    ];
    //
    function getVariants (codePoint)
    {
        let variantCodePoints = [ ];
        let codePointData = { ...unihanData.codePoints[codePoint], ...japaneseVariants[codePoint] };
        // Unicode Variants
        for (let variantProperty of variantProperties)
        {
            if (variantProperty in codePointData)
            {
                let variants = codePointData[variantProperty];
                if (!Array.isArray (variants))
                {
                    variants = [ variants ];
                }
                for (let variant of variants)
                {
                    variant = variant.split ("<")[0];
                    if (!variantCodePoints.includes (variant))
                    {
                        variantCodePoints.push (variant);
                    }
                }
            }
        }
        // Yasuoka Variants
        let variants = yasuokaVariants[codePoint] || [ ];
        for (let variant of variants)
        {
            if (variant in unihanData.codePoints)
            {
                if (!variantCodePoints.includes (variant))
                {
                    variantCodePoints.push (variant);
                }
            }
        }
        return variantCodePoints;
    }
    //
    function findCharactersByMatch (regex, matchVariants)
    {
        let characterList = [ ];
        for (let codePoint of unihanData.fullSet)
        {
            let character = String.fromCodePoint (parseInt (codePoint.replace ("U+", ""), 16));
            if (regex.test (character))
            {
                characterList.push (character);
                if (matchVariants)
                {
                    let variants = getVariants (codePoint);
                    for (let variant of variants)
                    {
                        characterList.push (String.fromCodePoint (parseInt (variant.replace ("U+", ""), 16)));
                    }
                }
            }
            else if (matchVariants)
            {
                let variants = getVariants (codePoint);
                for (let variant of variants)
                {
                    if (regex.test (String.fromCodePoint (parseInt (variant.replace ("U+", ""), 16))))
                    {
                        characterList.push (character);
                    }
                }
            }
        }
        return [...new Set (characterList)].sort ((a, b) => a.codePointAt (0) - b.codePointAt (0));
    }
    //
    function updateMatchResults (hitCount, totalCount)
    {
        matchHitCount.textContent = hitCount;
        matchTotalCount.textContent = totalCount;
        matchResultsButton.disabled = (hitCount <= 0);
    }
    //
    let currentCharactersByMatch = [ ];
    //
    matchSearchButton.addEventListener
    (
        'click',
        (event) =>
        {
            if (!matchSearchString.classList.contains ('error'))
            {
                let searchString = matchSearchString.value;
                if (searchString)
                {
                    let regex = null;
                    try
                    {
                        regex = regexp.build (searchString, { useRegex: matchUseRegex.checked });
                    }
                    catch (e)
                    {
                    }
                    if (regex)
                    {
                        clearSearch (matchSearchData);
                        currentCharactersByMatch = findCharactersByMatch (regex, matchVariants.checked);
                        updateMatchResults (currentCharactersByMatch.length, unihanCount);
                        if (currentCharactersByMatch.length > 0)
                        {
                            matchParams.pageIndex = 0;
                            matchSearchData.appendChild (matchDataTable.create (currentCharactersByMatch, matchVariants.checked ? regex : null, matchParams));
                        }
                    }
                }
            }
            else
            {
                shell.beep ();
            }
        }
    );
    //
    let matchResultsMenu =
    Menu.buildFromTemplate
    (
        [
            {
                label: "Copy Search Results", // "Copy Search Results as String"
                click: () =>
                {
                    if (currentCharactersByMatch.length > 0)
                    {
                        clipboard.writeText (currentCharactersByMatch.join (""));
                    }
                }
            },
            {
                label: "Save Search Results...", // "Save Search Results to File"
                click: () =>
                {
                    saveResults (currentCharactersByMatch.join (""));
                }
            },
            { type: 'separator' },
            {
                label: "Clear Search Results",
                click: () =>
                {
                    clearSearch (matchSearchData);
                    currentCharactersByMatch = [ ];
                    updateMatchResults (currentCharactersByMatch.length, unihanCount);
                }
            }
        ]
    );
    //
    matchResultsButton.addEventListener
    (
        'click',
        (event) =>
        {
            pullDownMenus.popup (event.currentTarget, matchResultsMenu);
        }
    );
    //
    updateMatchResults (currentCharactersByMatch.length, unihanCount);
    //
    matchInstructions.open = prefs.matchInstructions;
    matchRegexExamples.open = prefs.matchRegexExamples;
    //
    matchReferences.open = prefs.matchReferences;
    //
    const matchRefLinks = require ('./match-ref-links.json');
    linksList (matchLinks, matchRefLinks);
    //
    const gridDataTable = require ('./grid-data-table.js');
    //
    gridSpecimenHistory = prefs.gridSpecimenHistory;
    //
    gridParams.pageSize = prefs.gridPageSize;
    gridParams.pageIndex = prefs.gridPageIndex;
    gridParams.observer = null;
    gridParams.root = unit;
    //
    const unihanBlocks = require ('./unihan-blocks.json');
    //
    const keyIndex = require ('../../lib/key-index.js');
    //
    const nameIndex = keyIndex.build (unihanBlocks, 'name', (a, b) => a.localeCompare (b));
    const firstIndex = keyIndex.build (unihanBlocks, 'first', (a, b) =>  parseInt (a, 16) - parseInt (b, 16));
    //
    let blocks = { };
    //
    unihanBlocks.forEach
    (
        block =>
        {
            block.key = `${block.first}-${block.last}`;
            blocks[block.key] = block;
            block.range = `U+${block.first}..U+${block.last}`;
            block.firstIndex = parseInt (block.first, 16);
            block.lastIndex = parseInt (block.last, 16);
            block.size = block.lastIndex - block.firstIndex + 1;
            block.characters = [ ];
            block.coreCount = 0;
            block.core2020Count = 0;
            block.fullCount = 0;
            for (let index = block.firstIndex; index <= block.lastIndex; index++)
            {
                let character = String.fromCodePoint (index);
                if (unihan.isUnihan (character))
                {
                    block.fullCount++;
                }
                block.characters.push (character);
            }
        }
    );
    //
    for (let codePoint of unihanData.coreSet)
    {
        let index = parseInt (codePoint.replace ("U+", ""), 16);
        for (let block of unihanBlocks)
        {
            if ((block.firstIndex <= index) && (index <= block.lastIndex))
            {
                block.coreCount++;
                break;
            }
        }
    }
    //
    for (let codePoint of unihanData.core2020Set)
    {
        let index = parseInt (codePoint.replace ("U+", ""), 16);
        for (let block of unihanBlocks)
        {
            if ((block.firstIndex <= index) && (index <= block.lastIndex))
            {
                block.core2020Count++;
                break;
            }
        }
    }
    //
    function updateGridResults (hitCount, totalCount)
    {
        gridHitCount.textContent = hitCount;
        gridTotalCount.textContent = totalCount;
        gridResultsButton.disabled = (hitCount <= 0);
    }
    //
    let currentCharactersByGrid = [ ];
    //
    function displayRangeTable (blockKey, highlightedCharacter)
    {
        while (gridSearchData.firstChild)
        {
            gridSearchData.firstChild.remove ();
        }
        let block = blocks[blockKey];
        let characters = [ ];
        for (let index = block.firstIndex; index <= block.lastIndex; index++)
        {
            characters.push (String.fromCodePoint (index));
        }
        currentCharactersByGrid = characters.filter (character => unihan.isUnihan (character));
        updateGridResults (block.fullCount, block.size);
        gridSearchData.appendChild (gridDataTable.create (block.characters, gridParams, highlightedCharacter));
    }
    //
    firstIndex.forEach
    (
        index =>
        {
            let block = unihanBlocks[index];
            let option = document.createElement ('option');
            option.value = block.key;
            option.textContent = block.range;
            option.title = block.name;
            gridSelectBlockRange.appendChild (option);
        }
    );
    //
    nameIndex.forEach
    (
        index =>
        {
            let block = unihanBlocks[index];
            let option = document.createElement ('option');
            option.value = block.key;
            option.textContent = block.name;
            option.title = block.range;
            gridSelectBlockName.appendChild (option);
        }
    );
    //
    gridSpecimen.addEventListener
    (
        'input',
        (event) =>
        {
            event.currentTarget.classList.remove ('invalid');
            if (event.currentTarget.value)
            {
                if (!unihan.validateUnihanInput (event.currentTarget.value))
                {
                    event.currentTarget.classList.add ('invalid');
                }
            }
        }
    );
    gridSpecimen.addEventListener
    (
        'keypress',
        (event) =>
        {
            if (event.key === 'Enter')
            {
                event.preventDefault ();
                gridGoButton.click ();
            }
        }
    );
    gridSpecimen.addEventListener
    (
        'keydown',
        (event) =>
        {
            if (event.altKey)
            {
                if (event.key === 'ArrowUp')
                {
                    event.preventDefault ();
                    if (gridSpecimenHistoryIndex === -1)
                    {
                        gridSpecimenHistorySave = event.currentTarget.value;
                    }
                    gridSpecimenHistoryIndex++;
                    if (gridSpecimenHistoryIndex > (gridSpecimenHistory.length - 1))
                    {
                        gridSpecimenHistoryIndex = (gridSpecimenHistory.length - 1);
                    }
                    if (gridSpecimenHistoryIndex !== -1)
                    {
                        event.currentTarget.value = gridSpecimenHistory[gridSpecimenHistoryIndex];
                        event.currentTarget.dispatchEvent (new Event ('input'));
                    }
                }
                else if (event.key === 'ArrowDown')
                {
                    event.preventDefault ();
                    gridSpecimenHistoryIndex--;
                    if (gridSpecimenHistoryIndex < -1)
                    {
                        gridSpecimenHistoryIndex = -1;
                        gridSpecimenHistorySave = null;
                    }
                    if (gridSpecimenHistoryIndex === -1)
                    {
                        if (gridSpecimenHistorySave !== null)
                        {
                            event.currentTarget.value = gridSpecimenHistorySave;
                            event.currentTarget.dispatchEvent (new Event ('input'));
                        }
                    }
                    else
                    {
                        event.currentTarget.value = gridSpecimenHistory[gridSpecimenHistoryIndex];
                        event.currentTarget.dispatchEvent (new Event ('input'));
                    }
                }
            }
        }
    );
    gridGoButton.addEventListener
    (
        'click',
        (event) =>
        {
            if (gridSpecimen.value)
            {
                let character = unihan.validateUnihanInput (gridSpecimen.value);
                if (character)
                {
                    let index = character.codePointAt (0);
                    let blockKey = null;
                    for (let block of unihanBlocks)
                    {
                        if ((block.firstIndex <= index) && (index <= block.lastIndex))
                        {
                            blockKey = block.key;
                            break;
                        }
                    }
                    if (blockKey)   // Better safe than sorry...
                    {
                        let indexOfUnihanCharacter = gridSpecimenHistory.indexOf (character);
                        if (indexOfUnihanCharacter !== -1)
                        {
                            gridSpecimenHistory.splice (indexOfUnihanCharacter, 1);
                        }
                        gridSpecimenHistory.unshift (character);
                        if ((gridSpecimenHistorySize > 0) && (gridSpecimenHistory.length > gridSpecimenHistorySize))
                        {
                            gridSpecimenHistory.pop ();
                        }
                        gridSpecimenHistoryIndex = -1;
                        gridSpecimenHistorySave = null;
                        gridSpecimen.value = "";
                        gridSpecimen.classList.remove ('invalid');
                        gridSpecimen.blur ();
                        gridSelectBlockRange.value = blockKey;
                        gridSelectBlockName.value = blockKey;
                        displayRangeTable (blockKey, character);
                    }
                }
                else
                {
                    shell.beep ();
                }
            }
            else
            {
                gridSpecimenHistoryIndex = -1;
                gridSpecimenHistorySave = null;
                displayRangeTable (gridSelectBlockRange.value);
            }
        }
    );
    //
    function insertSpecimen (menuItem)
    {
        gridSpecimen.value = menuItem.id;
        gridSpecimen.dispatchEvent (new Event ('input'));
        gridGoButton.click ();
    };
    //
    gridSpecimenHistoryButton.addEventListener
    (
        'click',
        (event) =>
        {
            let historyMenuTemplate = [ ];
            historyMenuTemplate.push ({ label: "Specimen History", enabled: false })
            // historyMenuTemplate.push ({ type: 'separator' })
            if (gridSpecimenHistory.length > 0)
            {
                for (let specimen of gridSpecimenHistory)
                {
                    historyMenuTemplate.push
                    (
                        {
                            label: `${specimen}${(process.platform === 'darwin') ? "\t" : "\xA0\xA0"}${unicode.characterToCodePoint (specimen)}`,
                            id: specimen,
                            toolTip: unicode.getCharacterBasicData (specimen).name,
                            click: insertSpecimen
                        }
                    );
                }
            }
            else
            {
                historyMenuTemplate.push ({ label: "(no history yet)", enabled: false });
            }
            let historyContextualMenu = Menu.buildFromTemplate (historyMenuTemplate);
            pullDownMenus.popup (event.currentTarget, historyContextualMenu, 0);
        }
    );
    //
    gridSelectBlockRange.value = prefs.gridSelectBlockRange;
    if (gridSelectBlockRange.selectedIndex < 0) // -1: no element is selected
    {
        gridSelectBlockRange.selectedIndex = 0;
    }
    //
    gridSelectBlockName.value = gridSelectBlockRange.value;
    displayRangeTable (gridSelectBlockName.value);
    //
    gridSelectBlockRange.addEventListener
    (
        'input',
        (event) =>
        {
            gridSelectBlockName.value = event.currentTarget.value;
            gridParams.pageIndex = 0;
            displayRangeTable (event.currentTarget.value);
        }
    );
    //
    gridSelectBlockName.addEventListener
    (
        'input',
        (event) =>
        {
            gridSelectBlockRange.value = event.currentTarget.value;
            gridParams.pageIndex = 0;
            displayRangeTable (event.currentTarget.value);
        }
    );
    //
    let gridResultsMenu =
    Menu.buildFromTemplate
    (
        [
            {
                label: "Copy Results", // "Copy Results as String"
                click: () =>
                {
                    if (currentCharactersByGrid.length > 0)
                    {
                        clipboard.writeText (currentCharactersByGrid.join (""));
                    }
                }
            },
            {
                label: "Save Results...", // "Save Results to File"
                click: () =>
                {
                    saveResults (currentCharactersByGrid.join (""));
                }
            }
        ]
    );
    //
    gridResultsButton.addEventListener
    (
        'click',
        (event) =>
        {
            pullDownMenus.popup (event.currentTarget, gridResultsMenu);
        }
    );
    //
    gridInstructions.open = prefs.gridInstructions;
    //
    gridUnihanBlocks.open = prefs.gridUnihanBlocks;
    //
    let blocksTable = require ('./blocks-table.js');
    gridBlocks.appendChild (blocksTable.create (unihanBlocks));
    //
    gridReferences.open = prefs.gridReferences;
    //
    const gridRefLinks = require ('./grid-ref-links.json');
    linksList (gridLinks, gridRefLinks);
};
//
module.exports.stop = function (context)
{
    function getCurrentTabName ()
    {
        let currentTabName = "";
        for (let tab of tabs)
        {
            if (tab.checked)
            {
                currentTabName = tab.parentElement.textContent;
                break;
            }
        }
        return currentTabName;
    }
    //
    let prefs =
    {
        tabName: getCurrentTabName (),
        //
        propertySelect: propertyCurrentProperty,
        propertyShowCategories: propertyShowCategories,
        propertySearchString: propertySearchString.value,
        propertyWholeWord: propertyWholeWord.checked,
        propertyUseRegex: propertyUseRegex.checked,
        propertyPageSize: propertyParams.pageSize,
        propertyInstructions: propertyInstructions.open,
        propertyRegexExamples: propertyRegexExamples.open,
        propertyReferences: propertyReferences.open,
        //
        matchSearchString: matchSearchString.value,
        matchVariants: matchVariants.checked,
        matchUseRegex: matchUseRegex.checked,
        matchPageSize: matchParams.pageSize,
        matchInstructions: matchInstructions.open,
        matchRegexExamples: matchRegexExamples.open,
        matchReferences: matchReferences.open,
        //
        gridSelectBlockRange: gridSelectBlockRange.value,
        gridSpecimenHistory: gridSpecimenHistory,
        gridPageSize: gridParams.pageSize,
        gridPageIndex: gridParams.pageIndex,
        gridInstructions: gridInstructions.open,
        gridUnihanBlocks: gridUnihanBlocks.open,
        gridReferences: gridReferences.open,
        //
        defaultFolderPath: defaultFolderPath
    };
    context.setPrefs (prefs);
};
//
