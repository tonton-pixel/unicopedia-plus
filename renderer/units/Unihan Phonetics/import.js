//
const unitId = 'unihan-phonetics-unit';
//
const unit = document.getElementById (unitId);
//
const historyButton = unit.querySelector ('.history-button');
const unihanInput = unit.querySelector ('.unihan-input');
const lookUpButton = unit.querySelector ('.look-up-button');
const characterReference = unit.querySelector ('.character-reference');
const sortSelect = unit.querySelector ('.sort-select');
const sheet = unit.querySelector ('.sheet');
//
const instructions = unit.querySelector ('.instructions');
const references = unit.querySelector ('.references');
const links = unit.querySelector ('.links');
//
const unihanHistorySize = 256;   // 0: unlimited
//
let unihanHistory = [ ];
let unihanHistoryIndex = -1;
let unihanHistorySave = null;
//
let currentUnihanCharacter;
//
module.exports.start = function (context)
{
    const { clipboard, shell } = require ('electron');
    const { getCurrentWindow, Menu } = require ('@electron/remote');
    //
    const currentWindow = getCurrentWindow ();
    //
    const pullDownMenus = require ('../../lib/pull-down-menus.js');
    const linksList = require ('../../lib/links-list.js');
    //
    const unicode = require ('../../lib/unicode/unicode.js');
    const unihan = require ('../../lib/unicode/unihan.js');
    let { codePoints } = require ('../../lib/unicode/parsed-unihan-data.js');
    //
    let phoneticGroups = require ('../../lib/unicode/get-phonetic-groups.js');
    //
    function phoneticGroupCompare (a, b)
    {
        let [ , aNumber, aLetter ] = a.match (/(\d+)([A-D]?)/);
        aLetter ||= " ";
        let [ , bNumber, bLetter ] = b.match (/(\d+)([A-D]?)/);
        bLetter ||= " ";
        return ((aNumber - bNumber) || (aLetter - bLetter));
    }
    let sortedPhoneticGroupKeys = Object.keys (phoneticGroups).sort (phoneticGroupCompare);
    //
    const defaultPrefs =
    {
        unihanHistory: [ ],
        unihanCharacter: "",
        sortSelect: "",
        instructions: true,
        references: false
    };
    let prefs = context.getPrefs (defaultPrefs);
    //
    unihanHistory = prefs.unihanHistory;
    //
    sortSelect.value = prefs.sortSelect;
    if (sortSelect.selectedIndex < 0) // -1: no element is selected
    {
        sortSelect.selectedIndex = 0;
    }
    sortSelect.addEventListener
    (
        'input',
        event =>
        {
            if (currentUnihanCharacter)
            {
                updateUnihanData (currentUnihanCharacter);
            }
        }
    );
    //
    let currentCharacter;
    //
    let characterMenuTemplate =
    [
        { label: "Copy Character", click: (menuItem) => clipboard.writeText (currentCharacter) },
        { label: "Copy Code Point", click: (menuItem) => clipboard.writeText (unicode.characterToCodePoint (currentCharacter)) }
    ];
    let characterContextualMenu = Menu.buildFromTemplate (characterMenuTemplate);
    //
    function showCharacterMenu (event)
    {
        let phoneticsChar = event.target.closest ('.phonetics-char');
        if (phoneticsChar)
        {
            event.preventDefault ();
            currentCharacter = phoneticsChar.textContent;
            characterContextualMenu.popup ({ window: currentWindow });
        }
    }
    //
    function onLinkClick (event)
    {
        let clickable = event.target.closest ('.clickable');
        if (clickable)
        {
            event.preventDefault ();
            // updateUnihanData (clickable.dataset.char);
            updateUnihanData (clickable.textContent);
        }
    }
    //
    function getCodePoint (character)
    {
        return character.codePointAt (0);
    }
    //
    const codePointsSortFunc = (a, b) => getCodePoint (a) - getCodePoint (b);
    //
    const totalStrokesSortFunc = (a, b) => (unihan.getTotalStrokes (a) - unihan.getTotalStrokes (b)) || (getCodePoint (a) - getCodePoint (b));
    //
    const radicalStrokesSortFunc = (a, b) => (unihan.getRSCollationKey (a) - unihan.getRSCollationKey (b)) || (unihan.getTotalStrokes (a) - unihan.getTotalStrokes (b)) || (getCodePoint (a) - getCodePoint (b));
    //
    let readingSeparator = ", "; // "/", "･", "・", "•"
    let noReading = "(･･･)"; // "\xA0", "- - -", -----", "---", "--", "——", "･･･", "• • •", "•••", "***", "~~~", "< >", "(･･･)", "[･･･]", "∅"
    //
    function getReadings (character, language)
    {
        let readings = noReading;
        let codePoint = unicode.characterToCodePoint (character);
        let properties = codePoints[codePoint];
        switch (language)
        {
            case 'mandarin':
            default:
                if ("kMandarin" in properties)
                {
                    let mandarin = properties["kMandarin"];
                    readings = Array.isArray (mandarin) ? mandarin.join (readingSeparator) : mandarin;
                }
                break;
            case 'japanese':
                    let japanese = properties["kJapanese"];
                    if (!Array.isArray (japanese))
                    {
                        japanese = [ japanese ];
                    }
                    japanese = japanese.filter (onYomi => /^\p{Script=Katakana}+$/u.test (onYomi));
                    if (japanese.length > 0)
                    {
                        readings = japanese.join (readingSeparator);
                    }
                break;
        }
        return readings;
    }
    //
    function displayData (character)
    {
        function displayCharacterData (character, codePoint)
        {
            if (character)
            {
                let sortFunc;
                switch (sortSelect.value)
                {
                    case 'code-points':
                    default:
                        sortFunc = codePointsSortFunc
                        break;
                    case 'total-strokes':
                        sortFunc = totalStrokesSortFunc;
                        break;
                    case 'radical-strokes':
                        sortFunc = radicalStrokesSortFunc;
                        break;
                }
                let characterData = document.createElement ('div');
                characterData.className = 'character-data';
                //
                let characterInfo = document.createElement ('span');
                characterInfo.className = 'character-info';
                let characterGlyph = document.createElement ('span');
                characterGlyph.className = 'character-glyph';
                characterGlyph.textContent = character;
                characterGlyph.title = unihan.getExpandedTooltip (character);
                characterInfo.appendChild (characterGlyph);
                let characterCodePoint = document.createElement ('span');
                characterCodePoint.className = 'character-code-point';
                characterCodePoint.textContent = codePoint;
                characterInfo.appendChild (characterCodePoint);
                characterReference.appendChild (characterInfo);
                //
                let unihanCard = document.createElement ('div');
                unihanCard.className = 'unihan-card';
                let unihanWrapper = document.createElement ('div');
                unihanWrapper.className = 'unihan-wrapper';
                let unihanCharacter = document.createElement ('div');
                unihanCharacter.textContent = character;
                unihanCharacter.className = 'unihan-character';
                unihanWrapper.appendChild (unihanCharacter);
                let indexOfUnihanCharacter = unihanHistory.indexOf (unihanCharacter.textContent);
                if (indexOfUnihanCharacter !== -1)
                {
                    unihanHistory.splice (indexOfUnihanCharacter, 1);
                }
                unihanHistory.unshift (unihanCharacter.textContent);
                if ((unihanHistorySize > 0) && (unihanHistory.length > unihanHistorySize))
                {
                    unihanHistory.pop ();
                }
                unihanHistoryIndex = -1;
                unihanHistorySave = null;
                //
                let phonetics = document.createElement ('table');
                phonetics.className = 'phonetics';
                for (let phoneticGroup of sortedPhoneticGroupKeys)
                {
                    let groupsCharacters = phoneticGroups[phoneticGroup].map (char => char.replace ("*", ""));
                    if (groupsCharacters.includes (character))
                    {
                        groupsCharacters.sort (sortFunc);
                        let phoneticsRow = document.createElement ('tr');
                        phoneticsRow.className = 'phonetics-row';
                        let phoneticsGroup = document.createElement ('td');
                        phoneticsGroup.className = 'phonetics-group';
                        let phoneticsGroupString = document.createElement ('span');
                        phoneticsGroupString.className = 'phonetics-group-string';
                        phoneticsGroupString.textContent = phoneticGroup;
                        phoneticsGroup.appendChild (phoneticsGroupString);
                        phoneticsRow.appendChild (phoneticsGroup);
                        let phoneticsChars = document.createElement ('td');
                        phoneticsChars.className = 'phonetics-chars';
                        for (let groupsCharacter of groupsCharacters)
                        {
                            let readings = getReadings (groupsCharacter, 'mandarin');
                            let phoneticsCombo = document.createElement ('div');
                            phoneticsCombo.className = 'combo';
                            let phoneticsReadings = document.createElement ('span');
                            phoneticsReadings.className = 'phonetics-readings';
                            phoneticsReadings.textContent = readings;
                            if (readings === noReading)
                            {
                                phoneticsReadings.classList.add ('no-reading');
                                phoneticsReadings.title = "No registered reading";
                            }
                            else
                            {
                                phoneticsReadings.title = readings;
                            }
                            phoneticsCombo.appendChild (phoneticsReadings);
                            let phoneticsChar = document.createElement ('span');
                            phoneticsChar.className = 'phonetics-char';
                            if (groupsCharacter !== character)
                            {
                               phoneticsChar.classList.add ('clickable');
                            }
                            phoneticsChar.textContent = groupsCharacter;
                            phoneticsChar.title = unihan.getExpandedTooltip (groupsCharacter);
                            phoneticsCombo.appendChild (phoneticsChar);
                            let phoneticsCodePoint = document.createElement ('span');
                            phoneticsCodePoint.className = 'phonetics-code-point';
                            phoneticsCodePoint.textContent = unicode.characterToCodePoint (groupsCharacter, true);
                            phoneticsCombo.appendChild (phoneticsCodePoint);
                            phoneticsChars.appendChild (phoneticsCombo);
                        }
                        phoneticsRow.addEventListener ('contextmenu', showCharacterMenu);
                        phoneticsRow.addEventListener ('click', onLinkClick);
                        phoneticsRow.appendChild (phoneticsChars);
                        phonetics.appendChild (phoneticsRow);
                    }
                }
                sheet.appendChild (phonetics);
            }
        }
        //
        while (characterReference.firstChild)
        {
            characterReference.firstChild.remove ();
        }
        //
        while (sheet.firstChild)
        {
            sheet.firstChild.remove ();
        }
        currentUnihanCharacter = character;
        if (character)
        {
            let codePoint = unicode.characterToCodePoint (character);
            displayCharacterData (character, codePoint);
        }
    }
    //
    unihanInput.addEventListener
    (
        'input',
        (event) =>
        {
            event.currentTarget.classList.remove ('invalid');
            if (event.currentTarget.value)
            {
                if (!unihan.validateUnifiedInput (event.currentTarget.value))
                {
                    event.currentTarget.classList.add ('invalid');
                }
            }
        }
    );
    unihanInput.addEventListener
    (
        'keypress',
        (event) =>
        {
            if (event.key === 'Enter')
            {
                event.preventDefault ();
                lookUpButton.click ();
            }
        }
    );
    unihanInput.addEventListener
    (
        'keydown',
        (event) =>
        {
            if (event.altKey)
            {
                if (event.key === 'ArrowUp')
                {
                    event.preventDefault ();
                    if (unihanHistoryIndex === -1)
                    {
                        unihanHistorySave = event.currentTarget.value;
                    }
                    unihanHistoryIndex++;
                    if (unihanHistoryIndex > (unihanHistory.length - 1))
                    {
                        unihanHistoryIndex = (unihanHistory.length - 1);
                    }
                    if (unihanHistoryIndex !== -1)
                    {
                        event.currentTarget.value = unihanHistory[unihanHistoryIndex];
                        event.currentTarget.dispatchEvent (new Event ('input'));
                    }
                }
                else if (event.key === 'ArrowDown')
                {
                    event.preventDefault ();
                    unihanHistoryIndex--;
                    if (unihanHistoryIndex < -1)
                    {
                        unihanHistoryIndex = -1;
                        unihanHistorySave = null;
                    }
                    if (unihanHistoryIndex === -1)
                    {
                        if (unihanHistorySave !== null)
                        {
                            event.currentTarget.value = unihanHistorySave;
                            event.currentTarget.dispatchEvent (new Event ('input'));
                        }
                    }
                    else
                    {
                        event.currentTarget.value = unihanHistory[unihanHistoryIndex];
                        event.currentTarget.dispatchEvent (new Event ('input'));
                    }
                }
            }
        }
    );
    //
    function updateUnihanData (character)
    {
        unihanInput.value = "";
        unihanInput.blur ();
        unihanInput.dispatchEvent (new Event ('input'));
        displayData (character);
        unit.scrollTop = 0;
        unit.scrollLeft = 0;
    }
    //
    lookUpButton.addEventListener
    (
        'click',
        (event) =>
        {
            if (unihanInput.value)
            {
                let character = unihan.validateUnifiedInput (unihanInput.value);
                if (character)
                {
                    updateUnihanData (character);
                }
                else
                {
                    shell.beep ();
                }
            }
            else
            {
                unihanHistoryIndex = -1;
                unihanHistorySave = null;
                updateUnihanData ("");
            }
        }
    );
    //
    function insertUnihanCharacter (menuItem)
    {
        unihanInput.value = menuItem.id;
        unihanInput.dispatchEvent (new Event ('input'));
        lookUpButton.click ();
    };
    historyButton.addEventListener
    (
        'click',
        (event) =>
        {
            let historyMenuTemplate = [ ];
            historyMenuTemplate.push ({ label: "Lookup History", enabled: false })
            // historyMenuTemplate.push ({ type: 'separator' })
            if (unihanHistory.length > 0)
            {
                for (let unihan of unihanHistory)
                {
                    historyMenuTemplate.push
                    (
                        {
                            label: `${unihan}${(process.platform === 'darwin') ? "\t" : "\xA0\xA0"}${unicode.characterToCodePoint (unihan)}`,
                            id: unihan,
                            toolTip: unicode.getCharacterBasicData (unihan).name,
                            click: insertUnihanCharacter
                        }
                    );
                }
            }
            else
            {
                historyMenuTemplate.push ({ label: "(no history yet)", enabled: false });
            }
            let historyContextualMenu = Menu.buildFromTemplate (historyMenuTemplate);
            pullDownMenus.popup (event.currentTarget, historyContextualMenu, 0);
        }
    );
    //
    currentUnihanCharacter = prefs.unihanCharacter;
    updateUnihanData (currentUnihanCharacter);
    //
    const refLinks = require ('./ref-links.json');
    //
    instructions.open = prefs.instructions;
    //
    references.open = prefs.references;
    //
    linksList (links, refLinks);
};
//
module.exports.stop = function (context)
{
    let prefs =
    {
        unihanHistory: unihanHistory,
        unihanCharacter: currentUnihanCharacter,
        sortSelect: sortSelect.value,
        instructions: instructions.open,
        references: references.open
    };
    context.setPrefs (prefs);
};
//
