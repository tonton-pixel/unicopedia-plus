//
const unit = document.getElementById ('unikemet-inspector-unit');
//
const historyButton = unit.querySelector ('.history-button');
const unikemetInput = unit.querySelector ('.unikemet-input');
const lookUpButton = unit.querySelector ('.look-up-button');
const randomSetSelect = unit.querySelector ('.random-set-select');
const randomButton = unit.querySelector ('.random-button');
const infoContainer = unit.querySelector ('.info-container');
//
const instructions = unit.querySelector ('.instructions');
const references = unit.querySelector ('.references');
const links = unit.querySelector ('.links');
//
const unikemetHistorySize = 128;   // 0: unlimited
//
let unikemetHistory = [ ];
let unikemetHistoryIndex = -1;
let unikemetHistorySave = null;
//
let currentUnikemetCharacter;
//
let showCategories;
//
module.exports.start = function (context)
{
    const { shell } = require ('electron');
    const { Menu } = require ('@electron/remote');
    //
    const pullDownMenus = require ('../../lib/pull-down-menus.js');
    const linksList = require ('../../lib/links-list.js');
    //
    const unicode = require ('../../lib/unicode/unicode.js');
    const unikemet = require ('../../lib/unicode/unikemet.js');
    const unikemetData = require ('../../lib/unicode/parsed-unikemet-data.js');
    //
    const defaultPrefs =
    {
        unikemetHistory: [ ],
        unikemetCharacter: "",
        randomSetSelect: "",
        showCategories: false,
        instructions: true,
        references: false
    };
    let prefs = context.getPrefs (defaultPrefs);
    //
    unikemetHistory = prefs.unikemetHistory;
    //
    randomSetSelect.value = prefs.randomSetSelect;
    if (randomSetSelect.selectedIndex < 0) // -1: no element is selected
    {
        randomSetSelect.selectedIndex = 0;
    }
    //
    showCategories = prefs.showCategories;
    //
    //
    function getTooltip (character)
    {
        let data = unicode.getCharacterBasicData (character);
        return `${data.codePoint.replace (/U\+/, "U\u034F\+")}\xA0${character}`; // U+034F COMBINING GRAPHEME JOINER
    }
    function onLinkClick (event)
    {
        let clickable = event.target.closest ('.clickable');
        if (clickable)
        {
            event.preventDefault ();
            updateUnikemetData (clickable.dataset.char);
        }
    }
    //
    const sourcePattern = "\\b((?:[A-IK-Z]|Aa|NL|NU|Ff)[0-9]{1,3}[A-Za-z]{0,5}|(?:US1|US22|US248|US685)(?:[A-IK-Z]|Aa|NL|NU)[0-9]{1,3}[A-Za-z]{0,5})\\b"; // Whole-word JSesh source pattern, hard-coded for the time being...
    const sourceRegex = new RegExp (sourcePattern, 'gu');
    //
    function appendTextWithLinks (node, text, refLinks)
    {
        if (refLinks)
        {
            let matches = text.matchAll (sourceRegex);
            let clickables = [ ];
            for (let match of matches)
            {
                let matched = match[0];
                let index = match.index;
                let lastIndex = index + matched.length;
                let codePoint;
                let char;
                codePoint = unikemetData.references[match[1]];
                if (codePoint)
                {
                    char = String.fromCodePoint (parseInt (codePoint.replace ("U+", ""), 16));
                    if (unikemet.isUnikemet (char))
                    {
                        clickables.push ({ type: 'code-point', matched, index, lastIndex, codePoint, char });
                    }
                }
            }
            let lastIndex = 0;
            for (clickable of clickables)
            {
                node.appendChild (document.createTextNode (text.slice (lastIndex, clickable.index)));
                lastIndex = clickable.lastIndex;
                link = document.createElement ('span');
                link.className = 'unikemet-character-link';
                if (clickable.char != currentUnikemetCharacter)
                {
                    link.classList.add ('clickable');
                    link.dataset.char = clickable.char;
                }
                link.textContent = clickable.matched;
                link.title = getTooltip (clickable.char);
                node.appendChild (link);
            }
            node.addEventListener ('click', onLinkClick);
            node.appendChild (document.createTextNode (text.slice (lastIndex, text.length)));
        }
        else
        {
            // node.appendChild (document.createTextNode (text));
            node.textContent = text;
        }
    }
    //
    function appendFields (node, fieldItems)
    {
        for (let fieldItem of fieldItems)
        {
            if (!fieldItem)
            {
                let lineBreak = document.createElement ('br');
                node.appendChild (lineBreak);
            }
            else if (fieldItem.value)
            {
                let field = document.createElement ('div');
                field.className = 'field';
                if (Array.isArray (fieldItem.value))
                {
                    if (fieldItem.value.length > 0)
                    {
                        let name = document.createElement ('span');
                        name.className = 'name';
                        name.textContent = fieldItem.name.replace (/ /g, "\xA0");
                        field.appendChild (name);
                        field.appendChild (document.createTextNode (": "));
                        let value = document.createElement ('span');
                        value.className = 'value';
                        let list = document.createElement ('ul');
                        list.className = 'list';
                        fieldItem.value.forEach
                        (
                            (element, index) =>
                            {
                                let item = document.createElement ('li');
                                item.className = 'item';
                                if (Array.isArray (fieldItem.class))
                                {
                                    let itemClass = fieldItem.class[index];
                                    if (itemClass)
                                    {
                                        item.classList.add (itemClass);
                                    }
                                }
                                appendTextWithLinks (item, element, fieldItem.hasReferences);
                                list.appendChild (item);
                            }
                        );
                        value.appendChild (list);
                        field.appendChild (value);
                    }
                }
                else
                {
                    let name = document.createElement ('span');
                    name.className = 'name';
                    name.textContent = fieldItem.name.replace (/ /g, "\xA0");
                    field.appendChild (name);
                    field.appendChild (document.createTextNode (": "));
                    let value = document.createElement ('span');
                    value.className = 'value';
                    if (fieldItem.tooltip)
                    {
                        value.textContent = fieldItem.value;
                        value.title = fieldItem.tooltip;
                    }
                    else
                    {
                        appendTextWithLinks (value, fieldItem.value, fieldItem.hasReferences);
                    }
                    if (typeof fieldItem.class === 'string')
                    {
                        value.classList.add (fieldItem.class);
                    }
                    field.appendChild (value);
                }
                node.appendChild (field);
            }
        }
    }
    //
    function displayData (character)
    {
        function displayCharacterData (character, codePoint, properties)
        {
            let characterData = document.createElement ('div');
            characterData.className = 'character-data';
            //
            let unikemetCard = document.createElement ('div');
            unikemetCard.className = 'unikemet-card';
            let unikemetWrapper = document.createElement ('div');
            unikemetWrapper.className = 'unikemet-wrapper';
            let unikemetCharacter = document.createElement ('div');
            unikemetCharacter.textContent = character;
            unikemetCharacter.className = 'unikemet-character';
            unikemetWrapper.appendChild (unikemetCharacter);
            let indexOfUnikemetCharacter = unikemetHistory.indexOf (unikemetCharacter.textContent);
            if (indexOfUnikemetCharacter !== -1)
            {
                unikemetHistory.splice (indexOfUnikemetCharacter, 1);
            }
            unikemetHistory.unshift (unikemetCharacter.textContent);
            if ((unikemetHistorySize > 0) && (unikemetHistory.length > unikemetHistorySize))
            {
                unikemetHistory.pop ();
            }
            unikemetHistoryIndex = -1;
            unikemetHistorySave = null;
            let unikemetCodePoint= document.createElement ('div');
            unikemetCodePoint.textContent = codePoint;
            unikemetCodePoint.className = 'unikemet-code-point';
            unikemetWrapper.appendChild (unikemetCodePoint);
            unikemetCard.appendChild (unikemetWrapper);
            //
            let unicodeData = unicode.getCharacterData (character);
            let age = unicodeData.age && `Unicode ${unicodeData.age} (${unicodeData.ageDate})`;
            let unicodeFields =
            [
                { name: "Name", value: unicodeData.name },
                { name: "Age", value: age },
                { name: "Plane", value: unicodeData.planeName, tooltip: unicodeData.planeRange },
                { name: "Block", value: unicodeData.blockName, tooltip: unicodeData.blockRange },
                { name: "Script", value: unicodeData.script },
                { name: "Script Extensions", value: unicodeData.scriptExtensions },
                { name: "General Category", value: unicodeData.category },
                { name: "Extended Properties", value: unicodeData.extendedProperties }
            ];
            //
            let unicodeInfo = document.createElement ('div');
            unicodeInfo.className = 'unicode-info';
            for (let unicodeField of unicodeFields)
            {
                if (unicodeField.value)
                {
                    let field = document.createElement ('div');
                    field.className = 'field';
                    let name = document.createElement ('span');
                    name.className = 'name';
                    name.textContent = unicodeField.name.replace (/ /g, "\xA0");
                    field.appendChild (name);
                    field.appendChild (document.createTextNode (": "));
                    let value = document.createElement ('span');
                    value.className = 'value';
                    let text = Array.isArray (unicodeField.value) ? unicodeField.value.join (", ") : unicodeField.value;
                    value.textContent = text;
                    if (unicodeField.tooltip)
                    {
                        value.title = unicodeField.tooltip;
                    }
                    field.appendChild (value);
                    unicodeInfo.appendChild (field);
                }
            }
            //
            let unikemetInfo = document.createElement ('div');
            unikemetInfo.className = 'unikemet-info';
            if (properties)
            {
                let set = "Full Unikemet";;
                if (properties["kEH_Core"] === "C")
                {
                    set = "Core Unikemet";
                }
                let catalogValue = properties["kEH_Cat"];
                let [ groupCode, subgroupCode ] = catalogValue.split ("-");
                let groupValue = `${groupCode}. ${unikemetData.groups[groupCode].label}`;
                let subgroup = unikemetData.groups[groupCode].subgroups[subgroupCode];
                let subgroupValue = `${groupCode}${subgroupCode}. ${subgroup}`;
                let descriptionValue = properties["kEH_Desc"];
                let unikemetFields =
                [
                    { name: "Set", value: set },
                    { name: "Group", value: groupValue },
                    { name: "Subgroup", value: subgroupValue },
                    { name: "Description", value: descriptionValue, hasReferences: true },
                ];
                appendFields (unikemetInfo, unikemetFields);
            }
            else
            {
                let invalidUnikemet = document.createElement ('div');
                invalidUnikemet.className = 'field invalid-unikemet';
                // "Not a Unikemet character"
                // "Not a valid Unikemet character"
                // "No Unikemet character information"
                invalidUnikemet.textContent = "No Unikemet information available";
                unikemetInfo.appendChild (invalidUnikemet);
            }
            //
            characterData.appendChild (unikemetCard);
            characterData.appendChild (unicodeInfo);
            characterData.appendChild (unikemetInfo);
            infoContainer.appendChild (characterData);
        }
        //
        function displayProperties (properties)
        {
            function createPropertiesList (showCategories)
            {
                let propertiesList = document.createElement ('table');
                propertiesList.className = 'properties-list';
                let headerRow = document.createElement ('tr');
                headerRow.className = 'header-row';
                let propertyHeader = document.createElement ('th');
                propertyHeader.className = 'property-header';
                propertyHeader.textContent = "Unikemet\xA0Property";
                headerRow.appendChild (propertyHeader);
                let valueHeader = document.createElement ('th');
                valueHeader.className = 'value-header';
                valueHeader.textContent = "Value";
                headerRow.appendChild (valueHeader);
                propertiesList.appendChild (headerRow);
                if (showCategories)
                {
                    for (let category of unikemetData.categories)
                    {
                        let categoryProperties = category.properties.filter (property => (property in properties));
                        if (categoryProperties.length > 0)
                        {
                            let categoryRow = document.createElement ('tr');
                            categoryRow.className = 'category-row';
                            let categoryName = document.createElement ('td');
                            categoryName.className = 'category-name';
                            categoryName.textContent = category.name;
                            categoryName.colSpan = 2;
                            categoryRow.appendChild (categoryName);
                            propertiesList.appendChild (categoryRow);
                            for (let property of categoryProperties)
                            {
                                let value = properties[property];
                                if (value)
                                {
                                    let propertyRow = document.createElement ('tr');
                                    let propertyCell = document.createElement ('td');
                                    propertyCell.className = 'property';
                                    propertyCell.textContent = property;
                                    propertyCell.title = unikemetData.properties[property].name;
                                    propertyRow.appendChild (propertyCell);
                                    let valueCell = document.createElement ('td');
                                    valueCell.className = 'value';
                                    if (Array.isArray (value))
                                    {
                                        let list = document.createElement ('ul');
                                        list.className = 'list';
                                        for (let element of value)
                                        {
                                            let item = document.createElement ('li');
                                            item.className = 'item';
                                            appendTextWithLinks (item, element, property === "kEH_Desc");
                                            list.appendChild (item);
                                        }
                                        valueCell.appendChild (list);
                                    }
                                    else
                                    {
                                        appendTextWithLinks (valueCell, value, property === "kEH_Desc");
                                    }
                                    propertyRow.appendChild (valueCell);
                                    propertiesList.appendChild (propertyRow);
                                }
                            }
                        }
                    }
                }
                else
                {
                    let sortedKeys = Object.keys (properties).sort ((a, b) => a.localeCompare (b));
                    for (let property of sortedKeys)
                    {
                        let propertyRow = document.createElement ('tr');
                        let value = properties[property];
                        let propertyCell = document.createElement ('td');
                        propertyCell.className = 'property';
                        propertyCell.textContent = property;
                        propertyCell.title = unikemetData.properties[property].name;
                        propertyRow.appendChild (propertyCell);
                        let valueCell = document.createElement ('td');
                        valueCell.className = 'value';
                        if (Array.isArray (value))
                        {
                            let list = document.createElement ('ul');
                            list.className = 'list';
                            for (let element of value)
                            {
                                let item = document.createElement ('li');
                                item.className = 'item';
                                appendTextWithLinks (item, element, property === "kEH_Desc");
                                list.appendChild (item);
                            }
                            valueCell.appendChild (list);
                        }
                        else
                        {
                            appendTextWithLinks (valueCell, value, property === "kEH_Desc");
                        }
                        propertyRow.appendChild (valueCell);
                        propertiesList.appendChild (propertyRow);
                    }
                }
                return propertiesList;
            }
            //
            if (properties)
            {
                let propertiesData = document.createElement ('div');
                propertiesData.className = 'properties-data';
                //
                let interface = document.createElement ('div');
                interface.className = 'interface';
                let optionGroup = document.createElement ('span');
                optionGroup.className = 'option-group';
                let categoriesOption = document.createElement ('span');
                categoriesOption.className = 'option';
                let categoriesLabel = document.createElement ('label');
                let categoriesCheckbox = document.createElement ('input');
                categoriesCheckbox.className = "categories-checkbox";
                categoriesCheckbox.type = 'checkbox';
                categoriesCheckbox.checked = showCategories;
                categoriesCheckbox.addEventListener
                (
                    'input',
                    event =>
                    {
                        showCategories = event.currentTarget.checked;
                        while (propertiesWrapper.firstChild)
                        {
                            propertiesWrapper.firstChild.remove ();
                        }
                        propertiesWrapper.appendChild (createPropertiesList (showCategories));
                    }
                );
                categoriesLabel.appendChild (categoriesCheckbox);
                let categoriesText = document.createTextNode ("\xA0Categories");
                categoriesLabel.appendChild (categoriesText);
                categoriesOption.appendChild (categoriesLabel);
                optionGroup.appendChild (categoriesOption);
                interface.appendChild (optionGroup);
                infoContainer.appendChild (interface);
                //
                let propertiesWrapper = document.createElement ('div');
                propertiesWrapper.appendChild (createPropertiesList (showCategories));
                propertiesData.appendChild (propertiesWrapper);
                //
                infoContainer.appendChild (propertiesData);
            }
        }
        //
        while (infoContainer.firstChild)
        {
            infoContainer.firstChild.remove ();
        }
        currentUnikemetCharacter = character;
        if (character)
        {
            let codePoint = unicode.characterToCodePoint (character);
            let properties = unikemetData.codePoints[codePoint];
            displayCharacterData (character, codePoint, properties);
            displayProperties (properties);
        }
    }
    //
    unikemetInput.addEventListener
    (
        'input',
        (event) =>
        {
            event.currentTarget.classList.remove ('invalid');
            if (event.currentTarget.value)
            {
                if (!unikemet.validateUnikemetInput (event.currentTarget.value))
                {
                    event.currentTarget.classList.add ('invalid');
                }
            }
        }
    );
    unikemetInput.addEventListener
    (
        'keypress',
        (event) =>
        {
            if (event.key === 'Enter')
            {
                event.preventDefault ();
                lookUpButton.click ();
            }
        }
    );
    unikemetInput.addEventListener
    (
        'keydown',
        (event) =>
        {
            if (event.altKey)
            {
                if (event.key === 'ArrowUp')
                {
                    event.preventDefault ();
                    if (unikemetHistoryIndex === -1)
                    {
                        unikemetHistorySave = event.currentTarget.value;
                    }
                    unikemetHistoryIndex++;
                    if (unikemetHistoryIndex > (unikemetHistory.length - 1))
                    {
                        unikemetHistoryIndex = (unikemetHistory.length - 1);
                    }
                    if (unikemetHistoryIndex !== -1)
                    {
                        event.currentTarget.value = unikemetHistory[unikemetHistoryIndex];
                        event.currentTarget.dispatchEvent (new Event ('input'));
                    }
                }
                else if (event.key === 'ArrowDown')
                {
                    event.preventDefault ();
                    unikemetHistoryIndex--;
                    if (unikemetHistoryIndex < -1)
                    {
                        unikemetHistoryIndex = -1;
                        unikemetHistorySave = null;
                    }
                    if (unikemetHistoryIndex === -1)
                    {
                        if (unikemetHistorySave !== null)
                        {
                            event.currentTarget.value = unikemetHistorySave;
                            event.currentTarget.dispatchEvent (new Event ('input'));
                        }
                    }
                    else
                    {
                        event.currentTarget.value = unikemetHistory[unikemetHistoryIndex];
                        event.currentTarget.dispatchEvent (new Event ('input'));
                    }
                }
            }
        }
    );
    //
    function updateUnikemetData (character)
    {
        unikemetInput.value = "";
        unikemetInput.blur ();
        unikemetInput.dispatchEvent (new Event ('input'));
        displayData (character);
        unit.scrollTop = 0;
        unit.scrollLeft = 0;
    }
    //
    lookUpButton.addEventListener
    (
        'click',
        (event) =>
        {
            if (unikemetInput.value)
            {
                let character = unikemet.validateUnikemetInput (unikemetInput.value);
                if (character)
                {
                    updateUnikemetData (character);
                }
                else
                {
                    shell.beep ();
                }
            }
            else
            {
                unikemetHistoryIndex = -1;
                unikemetHistorySave = null;
                updateUnikemetData ("");
            }
        }
    );
    //
    function insertUnikemetCharacter (menuItem)
    {
        unikemetInput.value = menuItem.id;
        unikemetInput.dispatchEvent (new Event ('input'));
        lookUpButton.click ();
    };
    historyButton.addEventListener
    (
        'click',
        (event) =>
        {
            let historyMenuTemplate = [ ];
            historyMenuTemplate.push ({ label: "Lookup History", enabled: false })
            // historyMenuTemplate.push ({ type: 'separator' })
            if (unikemetHistory.length > 0)
            {
                for (let unikemet of unikemetHistory)
                {
                    historyMenuTemplate.push
                    (
                        {
                            label: `${unikemet}${(process.platform === 'darwin') ? "\t" : "\xA0\xA0"}${unicode.characterToCodePoint (unikemet)}`,
                            id: unikemet,
                            toolTip: unicode.getCharacterBasicData (unikemet).name,
                            click: insertUnikemetCharacter
                        }
                    );
                }
            }
            else
            {
                historyMenuTemplate.push ({ label: "(no history yet)", enabled: false });
            }
            let historyContextualMenu = Menu.buildFromTemplate (historyMenuTemplate);
            pullDownMenus.popup (event.currentTarget, historyContextualMenu, 0);
        }
    );
    //
    currentUnikemetCharacter = prefs.unikemetCharacter;
    updateUnikemetData (currentUnikemetCharacter);
    //
    function randomElement (elements)
    {
        return elements [Math.floor (Math.random () * elements.length)];
    }
    //
    randomButton.addEventListener
    (
        'click',
        (event) =>
        {
            let set;
            if (randomSetSelect.value === "Core")
            {
                set = unikemetData.coreSet;
            }
            else if (randomSetSelect.value === "Full")
            {
                set = unikemetData.fullSet;
            }
            let codePoint = randomElement (set);
            updateUnikemetData (String.fromCodePoint (parseInt (codePoint.replace ("U+", ""), 16)));
        }
    );
    //
    instructions.open = prefs.instructions;
    //
    references.open = prefs.references;
    //
    const refLinks = require ('./ref-links.json');
    linksList (links, refLinks);
};
//
module.exports.stop = function (context)
{
    let prefs =
    {
        unikemetHistory: unikemetHistory,
        unikemetCharacter: currentUnikemetCharacter,
        randomSetSelect: randomSetSelect.value,
        showCategories: showCategories,
        instructions: instructions.open,
        references: references.open
    };
    context.setPrefs (prefs);
};
//
