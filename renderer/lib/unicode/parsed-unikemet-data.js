//
// https://www.unicode.org/reports/tr57/
//
// All data in the Unikemet database is stored in UTF-8 using Normalization Form C (NFC).
// Note, however, that the "Syntax" descriptions below, used for validation of property values,
// operate on Normalization Form D (NFD), primarily because that makes the regular expressions simpler.
//
const properties =
{
    "kEH_Cat":
    {
        "name": "IFAO Catalog entry",
        "category": "Catalog Indexes",
        "syntax": "([A-IK-Z]|AA)-[0-9]{2}-[0-9]{3}"
    },
    "kEH_Core":
    {
        "name": "Set (Core, Legacy, or None)",
        "category": "Core",
        "syntax": "C|L|N",
        "default": "N"
    },
    "kEH_Desc":
    {
        "name": "Detailed description",
        "category": "Description",
        "syntax": "[^\\t\"]+"
    },
    "kEH_Func":
    {
        "name": "Function type",
        "category": "Function",
        // "separator": "/",
        "syntax": "[^\\t\"]+"
    },
    "kEH_FVal":
    {
        "name": "Function value",
        "category": "Function",
        // "separator": "[/|]",
        // "syntax": "[ꜣꞽyꜥwbpfmnrhḥḫẖsšḳkgtṯdḏ./\\|-;\\(\\)\\s]+"
        "syntax": "[^\\t\"]+"
    },
    "kEH_HG":
    {
        "name": "Hieroglyphica source",
        "category": "Sources",
        "separator": " ",
        "syntax": "([A-IK-Z]|AA)[0-9]{1,3}[A-Za-z]{0,2}"
    },
    "kEH_IFAO":
    {
        "name": "IFAO source ",
        "category": "Sources",
        "separator": " ",
        // "syntax": "[0-9]{1,3},[0-9]{1,2}"
        "syntax": "[0-9]{1,3},[0-9]{1,2}[ab]?"
    },
    "kEH_JSesh":
    {
        "name": "JSesh source ",
        "category": "Sources",
        "separator": " ",
        "syntax": "([A-IK-Z]|Aa|NL|NU|Ff)[0-9]{1,3}[A-Za-z]{0,5}|(US1|US22|US248|US685)([A-IK-Z]|Aa|NL|NU)[0-9]{1,3}[A-Za-z]{0,5}"
    },
    "kEH_NoMirror":
    {
        "name": "Does not mirror? ",
        "category": "Mirroring and Rotation",
        "syntax": "Y|N",
        "default": "N"
    },
    "kEH_NoRotate":
    {
        "name": "Does not rotate? ",
        "category": "Mirroring and Rotation",
        "syntax": "Y|N",
        "default": "N"
    },
    "kEH_UniK":
    {
        "name": "Original Unikemet catalog index",
        "category": "Catalog Indexes",
        "syntax": "([A-IK-Z]|AA|NL|NU)[0-9]{2,3}[A-Z]{0,2}|HJ ([A-IK-Z]|AA)[0-9]{2,3}[A-Z]{0,2}"
    }
};
//
for (let property in properties)
{
    if ("separator" in properties[property])
    {
        properties[property]["separator_regex"] = new RegExp (properties[property]["separator"], 'u');
    }
    if ("syntax" in properties[property])
    {
        properties[property]["syntax_regex"] = new RegExp ( '^(' + properties[property]["syntax"] + ')$', 'u');
    }
}
//
// Property categories (field types)
//
const categories =
[
    {
        "name": "Catalog Indexes",
        "properties":
        [
            "kEH_Cat",
            "kEH_UniK"
        ]
    },
    {
        "name": "Core",
        "properties":
        [
            "kEH_Core"
        ]
    },
    {
        "name": "Description",
        "properties":
        [
            "kEH_Desc"
        ]
    },
    {
        "name": "Function",
        "properties":
        [
            "kEH_Func",
            "kEH_FVal"
        ]
    },
    {
        "name": "Mirroring and Rotation",
        "properties":
        [
            "kEH_NoMirror",
            "kEH_NoRotate"
        ]
    },
    {
        "name": "Sources",
        "properties":
        [
            "kEH_HG",
            "kEH_IFAO",
            "kEH_JSesh"
        ]
    }
];
//
function parseUnikemetProperty (codePoint, property, value)
{
    // https://www.unicode.org/reports/tr57/
    //
    // Validation is done as follows:
    // The entry is split into subentries using the Delimiter (if defined),
    // and each subentry converted to Normalization Form D (NFD).
    // The value is valid if and only if each normalized subentry matches the field’s Syntax regular expression.
    //
    let values;
    if (property in properties)
    {
        if ("separator_regex" in properties[property])
        {
            values = value.split (properties[property]["separator_regex"]);
        }
        else
        {
            values = [ value ];
        }
        values = values.map (value => value.trim ()).filter (value => value.length > 0);
        if ("syntax_regex" in properties[property])
        {
            let regex = properties[property]["syntax_regex"];
            values.forEach
            (
                value =>
                {
                    if (!regex.test (value.normalize ('NFD')))
                    {
                        console.log ("Invalid Syntax:", codePoint, property, value);
                    }
                }
            );
        }
    }
    //
    return (values.length > 1) ? values : values[0];
}
//
const fs = require ('fs');
const path = require ('path');
//
// Copy of https://www.unicode.org/Public/16.0.0/ucd/Unikemet.txt
//
const codePoints = { };
//
let lines = fs.readFileSync (path.join (__dirname, 'Unikemet', 'Unikemet.txt'), { encoding: 'utf8' }).split (/\r?\n/);
for (let line of lines)
{
    if (line && (line[0] !== "#"))
    {
        let found = line.match (/^(U\+1[0-9A-F]{4})\t(\w+)\t(.+)$/);
        if (found)
        {
            if (!(found[1] in codePoints))
            {
                codePoints[found[1]] = { };
            }
            codePoints[found[1]][found[2]] = parseUnikemetProperty (found[1], found[2], found[3]);
        }
    }
}
//
const fullSet = Object.keys (codePoints).sort ((a, b) => parseInt (a.replace ("U+", ""), 16) - parseInt (b.replace ("U+", ""), 16));
const coreSet = fullSet.filter (key => (codePoints[key]["kEH_Core"] === "C"));
//
// https://www.unicode.org/reports/tr57/
// https://www.unicode.org/wg2/docs/n5240R2-hieroglyphs.pdf
// https://www.unicode.org/Public/16.0.0/charts/CodeCharts.pdf
// https://www.unicode.org/charts/PDF/U13000.pdf
// https://www.unicode.org/charts/PDF/U13460.pdf
const groups =
{
    "A":
    {
        "label": "Man and his occupations",
        "subgroups":
        {
            "01": "Man seated or kneeling empty handed",
            "02": "Man standing empty handed",
            "03": "Man, head down", // ??
            "04": "Man adoring or bent",
            "05": "Man on the ground or in water",
            "06": "Man or god standing, holding a staff",
            "07": "Man seated or kneeling, holding something",
            "08": "Man standing, holding something",
            "09": "Man seated, pouring water",
            "10": "Man standing, pouring water, spreading powder",
            "11": "Man hiding",
            "12": "Man working",
            "13": "Shepherd and porter",
            "14": "Man carrying a bundle",
            "15": "Man or god standing, composed with a hieroglyphic sign",
            "16": "Man or god holding a weapon",
            "17": "Soldier, seated",
            "18": "Prisoner and enemy",
            "19": "Dancer and acrobat",
            "20": "Musician",
            "21": "Man and animal",
            "22": "Man in a boat",
            "23": "Child",
            "24": "Dwarf",
            "25": "King or noble seated on a chair",
            "26": "King or god seated without crown",
            "27": "King or god standing without crown",
            "28": "King or god wearing the white crown",
            "29": "King or god wearing the red crown",
            "30": "King or god wearing another crown",
            "31": "Dead person kneeling",
            "32": "Mummy",
            "33": "Statue",
            "34": "Man, varia"
        }
    },
    "B":
    {
        "label": "Woman and her occupations",
        "subgroups":
        {
            "01": "Woman seated",
            "02": "Woman kneeling",
            "03": "Woman kneeling with visible arms",
            "04": "Woman seated on a chair",
            "05": "Woman, standing",
            "06": "Queen or goddess, seated, wearing a crown",
            "07": "Queen or goddess, standing, wearing a crown",
            "08": "Woman, musician",
            "09": "Woman, queen or goddess pregnant, giving birth, breastfeeding",
            "10": "Woman, varia"
        }
    },
    "C":
    {
        "label": "Anthropomorphic deities",
        "subgroups":
        {
            "01": "God, seated, various headsets",
            "02": "God, various, with animal heads",
            "03": "Amon",
            "04": "Anubis and canides gods",
            "05": "Apis and bull head gods",
            "06": "Atoum", // ??
            "07": "Bes",
            "08": "Chou",
            "09": "Falcon, seated",
            "10": "Falcon, standing",
            "11": "Falcon, slaying an animal",
            "12": "Geb",
            "13": "Heh",
            "14": "Ihy and children gods",
            "15": "Khnoum and ram gods",
            "16": "Gods with lion head",
            "17": "Min",
            "18": "Nefertoum",
            "19": "Nil",
            "20": "Onouris",
            "21": "Orion",
            "22": "Osiris",
            "23": "Ptah",
            "24": "Re",
            "25": "Rechep",
            "26": "Seth",
            "27": "Sobek",
            "28": "Soped",
            "29": "Gods with monkey head",
            "30": "Tatenen",
            "31": "Thot",
            "32": "Gods, varia",
            "33": "Anoukis",
            "34": "Ermouthis and snake goddesses",
            "35": "Hathor/Isis",
            "36": "Lion gods and goddesses",
            "37": "Maat",
            "38": "Meret",
            "39": "Mut",
            "40": "Neith",
            "41": "Nekhbet",
            "42": "Nephthys",
            "43": "Nout",
            "44": "Satis",
            "45": "Seshat",
            "46": "Sothis",
            "47": "Goddesses, varia",
            "48": "Goddesses, gods group"
        }
    },
    "D":
    {
        "label": "Parts of the human body",
        "subgroups":
        {
            "01": "Head, sideways",
            "02": "Head, front",
            "03": "Head and arms",
            "04": "Hairs",
            "05": "Eye",
            "06": "Eye made up",
            "07": "Eye crying",
            "08": "Eye oudjat",
            "09": "Eye oudjat components",
            "10": "Ear",
            "11": "Face sideways",
            "12": "Mouth",
            "13": "Lips",
            "14": "Mouth spitting",
            "15": "Beard",
            "16": "Breast", // ??
            "17": "Lower part of kneeling or sitting man",
            "18": "Arms (straight)",
            "19": "Arms (bent)",
            "20": "Arm or arms holding an oar",
            "21": "Arm holding shield and weapons",
            "22": "Arm flat",
            "23": "Forearm (empty handed, palm up)",
            "24": "Forearm (holding loaf of bread or round vase with rim)",
            "25": "Forearm (holding stick, varia)",
            "26": "Arm bent",
            "27": "Arms bent, holding an object",
            "28": "Hand",
            "29": "Fist",
            "30": "Finger",
            "31": "Male genitals",
            "32": "Female genitals",
            "33": "Thighs", // ??
            "34": "Legs",
            "35": "Leg",
            "36": "Toes",
            "37": "Sole",
            "38": "Heqat measures", // ??
            "39": "Human parts, varia"
        }
    },
    "E":
    {
        "label": "Mammals",
        "subgroups":
        {
            "01": "Donkey",
            "02": "Sethian animal",
            "03": "Antelope",
            "04": "Ram and ewe",
            "05": "Bubale",
            "06": "Dog, jackal standing",
            "07": "Dog, jackal sitting",
            "08": "Cat",
            "09": "Horse",
            "10": "Goat and kid",
            "11": "Elephant", // ??
            "12": "Giraffe", // ??
            "13": "Griffin",
            "14": "Hippopotamus",
            "15": "Hyena",
            "16": "Hare",
            "17": "Lion",
            "18": "Panther",
            "19": "Pig",
            "20": "Monkey",
            "21": "Mouse",
            "22": "Sphinx",
            "23": "Bovid",
            "24": "", // ??
            "25": "Calf",
            "26": "Mammals, varia"
        }
    },
    "F":
    {
        "label": "Parts of mammals",
        "subgroups":
        {
            "01": "Donkey head",
            "02": "Ram head",
            "03": "Bovidae heads",
            "04": "Dog head",
            "05": "Animal heads on stick or leg",
            "06": "Cervidae heads",
            "07": "Hippopotamus head",
            "08": "Lion head and protome",
            "09": "Donkey and horse protome",
            "10": "Animal head, varia",
            "11": "Bovidae horns",
            "12": "Ram horns",
            "13": "Horn, single", // ??
            "14": "Elephant tusk",
            "15": "Jaws",
            "16": "Tongue",
            "17": "Ear",
            "18": "Hindquarter",
            "19": "Leg",
            "20": "Skin",
            "21": "Teat",
            "22": "Tail",
            "23": "Heart",
            "24": "Heart and lungs",
            "25": "Spine",
            "26": "Verterbrae and ribs",
            "27": "Haunch",
            "28": "Intestines",
            "29": "Flesh part", // ??
            "30": "Excrements, pustule",
            "31": "Placenta",
            "32": "Mammal parts, varia"
        }
    },
    "G":
    {
        "label": "Birds",
        "subgroups":
        {
            "01": "Ostrich",
            "02": "Buzzard",
            "03": "Quail, chick", // ??
            "04": "Duck",
            "05": "Duck, flying",
            "06": "Duck tied to a pole",
            "07": "Duck, crammed", // ??
            "08": "Goose",
            "09": "Owl",
            "10": "Cormorant",
            "11": "Wader: ibis, flamingo, stork, heron, egret",
            "12": "Falcon",
            "13": "Falcon opening his wings",
            "14": "Falcon legs bent",
            "15": "Falcon mummified or emblem of falcon",
            "16": "Swallow, sparrow",
            "17": "Hoopoe",
            "18": "Bird, human-headed",
            "19": "Bird, dog- or ram-headed",
            "20": "Chick or sitting duck",
            "21": "Pelican",
            "22": "Guinea fowl",
            "23": "Peewit, lapwing",
            "24": "Vulture",
            "25": "Vulture, with open wings",
            "26": "Vulture (Neophron percnopterus)",
            "27": "Bird, varia"
        }
    },
    "H":
    {
        "label": "Parts of birds",
        "subgroups":
        {
            "01": "Falcon head",
            "02": "Falcon eye",
            "03": "Duck/goose head",
            "04": "Crested bird head",
            "05": "Spoonbill head", // ??
            "06": "Vulture head",
            "07": "Wing",
            "08": "Feather",
            "09": "Leg",
            "10": "Egg",
            "11": "Bird part, varia"
        }
    },
    "I":
    {
        "label": "Amphibious animals, reptiles, etc.",
        "subgroups":
        {
            "01": "Lizard",
            "02": "Turtle",
            "03": "Crocodile",
            "04": "Crocodile parts",
            "05": "Frog, tadpole",
            "06": "Viper",
            "07": "Horned viper",
            "08": "Snake wrt hk.w",
            "09": "Cobra type 1 (I10)",
            "10": "Cobra type 2 (I40)",
            "11": "Cobra type 3 (I64)",
            "12": "Cobra type 4 (I80)",
            "13": "Cobra type 5 (I14/I15)",
            "14": "Snake head",
            "15": "Snake, varia"
        }
    },
    "K":
    {
        "label": "Fishes and parts of fishes",
        "subgroups":
        {
            "01": "Fishes",
            "02": "Fish parts"
        }
    },
    "L":
    {
        "label": "Invertebrata and lesser animals", // Invertebrate ??
        "subgroups":
        {
            "01": "Scarab",
            "02": "Bee, fly and wasp",
            "03": "Grasshopper, locust",
            "04": "Praying mantis",
            "05": "Centipede", // ??
            "06": "Scorpion and water bug",
            "07": "Insects, varia"
        }
    },
    "M":
    {
        "label": "Trees and plants",
        "subgroups":
        {
            "01": "Tree",
            "02": "Palm tree",
            "03": "Clump type 1",
            "04": "Branch",
            "05": "Stripped palm branch",
            "06": "Lotus on a pond",
            "07": "Lotus bud",
            "08": "Lotus flower",
            "09": "Composite flower bunch",
            "10": "Lotus with bent stem",
            "11": "Lotus leaf",
            "12": "Papyrus stem",
            "13": "Clump of papyrus and lily",
            "14": "Reed leaf",
            "15": "Clump of reeds",
            "16": "Rush with shoots",
            "17": "Carob pod",
            "18": "Root",
            "19": "Rhizome",
            "20": "Grains",
            "21": "Ear",
            "22": "Sheaf (flax bundle)",
            "23": "Basket of fruits or grains",
            "24": "Reed bundle",
            "25": "Wood log", // ??
            "26": "Vine arbor (grape vines on props)",
            "27": "Thorn", // ??
            "28": "Vegetal elements, varia"
        }
    },
    "N":
    {
        "label": "Sky, earth, water",
        "subgroups":
        {
            "01": "Sky",
            "02": "Sun",
            "03": "Sun with Uraeus",
            "04": "Sun, radiant, with rays",
            "05": "Sun with wings",
            "06": "Moon",
            "07": "Moon quarter", // ??
            "08": "Star",
            "09": "Flat land",
            "10": "Tongue of land",
            "11": "Canal, irrigated land",
            "12": "Irrigation channels", // ??
            "13": "Mountain range (desert)",
            "14": "Sand",
            "15": "Sunrise over mountain (horizon)",
            "16": "Rising sun",
            "17": "Sand slope, hill with shrubs (mound)",
            "18": "Road bordered with shrubs (road)",
            "19": "Water",
            "20": "Canal and pool",
            "21": "Well",
            "22": "Sky, ground, earth, varia"
        }
    },
    "O":
    {
        "label": "Buildings, parts of buildings, etc.",
        "subgroups":
        {
            "01": "House",
            "02": "Reed shelter, winding wall (enclosure)",
            "03": "Plan of rectangular enclosure",
            "04": "Palace with battlements",
            "05": "Enclosure with battlements",
            "06": "Palace or tomb façade",
            "07": "Shrine façade",
            "08": "Door and gateway with snake",
            "09": "Shrine",
            "10": "Open shelter",
            "11": "Double platform (sed festival)",
            "12": "Pyramid",
            "13": "Obelisk",
            "14": "Stela",
            "15": "Hall with columns", // ??
            "16": "Column (support)",
            "17": "Door flap", // ??
            "18": "Door bolt",
            "19": "Building angle",
            "20": "Stairway",
            "21": "Fence",
            "22": "Grain mound on mud floor",
            "23": "Frieze elements",
            "24": "Architecture elements"
        }
    },
    "P":
    {
        "label": "Ships and part of ships",
        "subgroups":
        {
            "01": "Boat",
            "02": "Sailboat",
            "03": "Sacred boat",
            "04": "Boat with net",
            "05": "Boat, varia",
            "06": "Sail",
            "07": "Mast",
            "08": "Oar",
            "09": "Steering oar",
            "10": "Mooring post",
            "11": "Steering gear",
            "12": "Bale", // ??
            "13": "Boat parts, varia"
        }
    },
    "Q":
    {
        "label": "Domestic and funerary furniture",
        "subgroups":
        {
            "01": "Seat",
            "02": "Headrest",
            "03": "Bed",
            "04": "Mirror and base",
            "05": "Chest and coffin",
            "06": "Brazier",
            "07": "Furniture, varia"
        }
    },
    "R":
    {
        "label": "Temple furniture and sacred emblems",
        "subgroups":
        {
            "01": "Table for offerings",
            "02": "Low table (dresser)",
            "03": "Altar",
            "04": "Bread loaf on a mat",
            "05": "Censer",
            "06": "Incense bowl",
            "07": "Flag",
            "08": "Reed column",
            "09": "Standard",
            "10": "Sun shade emblem", // ??,
            "11": "West or right side emblem",
            "12": "East or left side emblem",
            "13": "Emblem type R80",
            "14": "Emblem of eighth nome of Upper Egypt",
            "15": "Nefertoum emblem",
            "16": "Seth emblem",
            "17": "Seshat emblem",
            "18": "Min emblem",
            "19": "Neith emblem", // ??
            "20": "Sign mks",
            "21": "Emblem of seventh nome of Upper Egypt", // ??
            "22": "Shedet emblem",
            "23": "Temple furniture, varia"
        }
    },
    "S":
    {
        "label": "Crowns, dress, staves, etc.", // dresses ??
        "subgroups":
        {
            "01": "White crown of Upper Egypt",
            "02": "Red crown of Lower Egypt",
            "03": "Union of red crown of Lower Egypt and white crown of Upper Egypt",
            "04": "Blue crown",
            "05": "Head cloth",
            "06": "Band of head cloth with loop and ties at the back",
            "07": "Atef crown",
            "08": "Hemhem crown",
            "09": "Double plumes headdress",
            "10": "Headband",
            "11": "Collar",
            "12": "Seal",
            "13": "Knots, bracelets, garment with knots",
            "14": "Loincloth, apron",
            "15": "Cloth",
            "16": "Sandal", // ??
            "17": "Sandal strap, ankh",
            "18": "Isis knot", // ??,
            "19": "Sunshade type 1 (S35)", // ??
            "20": "Sunshade type 2 (S36)",
            "21": "Sunshade type 3 (S37D)",
            "22": "Sceptre type 1 (crook)",
            "23": "Sceptre type 2 (Seth animal)",
            "24": "Sceptre type 3 (S42)",
            "25": "Sticks, other sceptres",
            "26": "Flabellum",
            "27": "Seth tool",
            "28": "Collar counterweight, bag of clothes",
            "29": "S category, varia"
        }
    },
    "T":
    {
        "label": "Warfare, hunting, butchery",
        "subgroups":
        {
            "01": "Mace",
            "02": "Shield",
            "03": "Axe",
            "04": "Axe head",
            "05": "Dagger",
            "06": "Bow",
            "07": "Arrow, quiver, cover of a quiver",
            "08": "Bowstring",
            "09": "Throwing stick",
            "10": "Scimitar",
            "11": "Chariot", // ??
            "12": "Crook with package",
            "13": "Harpoon",
            "14": "Fishing net (sideway)",
            "15": "Reed float (Object db.)",
            "16": "Trap",
            "17": "Butcher's block", // ??
            "18": "Knife",
            "19": "Sharpener, butcher's knife",
            "20": "Knife pesesh-kaf",
            "21": "T category, varia"
        }
    },
    "U":
    {
        "label": "Agriculture, crafts, and professions",
        "subgroups":
        {
            "01": "Sickle",
            "02": "Hoe",
            "03": "Grain measure",
            "04": "Fork", // ??
            "05": "Plow",
            "06": "Sled (protome)",
            "07": "Planted Pick",
            "08": "Adze",
            "09": "Chisel and Drill",
            "10": "Fire drill", // ??
            "11": "Kiln and ingot",
            "12": "Pestle",
            "13": "Spindle (stopper)",
            "14": "Washing club",
            "15": "Razor",
            "16": "Scale, measuring instrument",
            "17": "Potter's wheel",
            "18": "Press (wine, oil), warp extended between poles",
            "19": "Riddle (coarse sieve)",
            "20": "Baker rake", // ??
            "21": "Mill tool",
            "22": "U category, varia"
        }
    },
    "V":
    {
        "label": "Rope, fiber, baskets, bags, etc.",
        "subgroups":
        {
            "01": "Rope and rope coils",
            "02": "Round cartouche, knot",
            "03": "Cartouche",
            "04": "String, tethering rope, hobble",
            "05": "Whip",
            "06": "Cord on stick",
            "07": "Spool with thread",
            "08": "Wick and swab",
            "09": "Basket",
            "10": "Woven basket",
            "11": "Wicker satchel",
            "12": "Linen bag", // ??
            "13": "Bag of linen with a hollow bottom", // ??
            "14": "Receptacle, bunch",
            "15": "Bandage, receptacle",
            "16": "Bushel",
            "17": "Net"
        }
    },
    "W":
    {
        "label": "Vessels of stone and earthenware",
        "subgroups":
        {
            "01": "Oil vessel",
            "02": "Alabaster basin",
            "03": "Stone jug (hnm)",
            "04": "Cup",
            "05": "Stand",
            "06": "Tall jar",
            "07": "Milk jug",
            "08": "Situla",
            "09": "Jug pouring liquid",
            "10": "Wine jars (twin jars)",
            "11": "Receptacles, varia"
        }
    },
    "X":
    {
        "label": "Loaves and cakes",
        "subgroups":
        {
            "01": "Small bread loaf", // ??
            "02": "Tall bread loaf",
            "03": "Bread type 1 (X3)",
            "04": "Bread roll type 2",
            "05": "Bread roll type 3 (X5)",
            "06": "Bread type 4 (X6)",
            "07": "Bread type 5 (X8)",  // ??
            "08": "Other breads"
        }
    },
    "Y":
    {
        "label": "Writings, games, music",
        "subgroups":
        {
            "01": "Papyrus scroll",
            "02": "Scribal kit, graver",
            "03": "Game board and piece (Senet)",
            "04": "Harp",
            "05": "Tambourine and ball",
            "06": "Sistrum"
        }
    },
    "Z":
    {
        "label": "Strokes, signs derived from Hieratic, geometrical figures",
        "subgroups":
        {
            "01": "Strokes",
            "02": "Cross",
            "03": "Squares and rectangles",
            "04": "Triangles, lozenges",
            "05": "Circles and rings", // ??
            "06": "Geometric shapes, varia",
            "07": "Hieratic signs"
        }
    },
    "AA": // "Aa" ??
    {
        "label": "Unclassified",
        "subgroups":
        {
            "01": "Sign type 1",
            "02": "Sign type 2",
            "03": "Sign type 3",
            "04": "Sign type 4",
            "05": "Sign type 5",
            "06": "Sign type 6",
            "07": "Sign type 7",
            "08": "Sign type 8",
            "09": "Sign type 9",
            "10": "Sign type 10 (Back of something?)",
            "11": "Sign type 11 (Collar counterweight?, bag of clothes)",
            "12": "Sign type 12",
            "13": "Sign type 13",
            "14": "Sign type 14",
            "15": "Sign type 15",
            "16": "Sign type 16",
            "17": "Sign type 17",
            "18": "Sign type 18",
            "19": "Sign type 19",
            "20": "Sign type 20",
            "21": "Sign type 21",
            "22": "Unclassified signs, varia",
            "23": "Unclassified signs, esna VII",
            "24": "Unclassified signs, Porte d'Horus"
        }
    }
};
//
// https://www.unicode.org/reports/tr57/
//
// Note that the description currently uses the Hieroglyphica/JSesh references in many of these descriptions to 
// designate another sign included in the sign. The example above, 'I64' refer to U+13D79 which is itself 
// described as 'A cobra (Naja haja), standing up, with expanded hood (Uraeus)'. Because Hieroglyphica 
// and JSesh do not always coincide, in case of differences, the JSesh reference prevails.
//
let references = { };
for (let codePoint in codePoints)
{
    let reference = codePoints[codePoint]["kEH_JSesh"];
    if (reference)
    {
        if (!Array.isArray (reference))
        {
            reference = [ reference ];
        }
        for (let referenceElement of reference)
        {
            if (referenceElement in references)
            {
                console.log ("Duplicate Source Reference:", referenceElement);
            }
            else
            {
                references[referenceElement] = codePoint;
            }
        }
    }
}
//
module.exports =
{
    properties,
    categories,
    codePoints,
    fullSet,
    coreSet,
    groups,
    references
};
//
