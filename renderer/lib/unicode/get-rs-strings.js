//
const kangxiRadicals = require ('./kangxi-radicals.json');
//
function fromRadical (index, marker, verbose)
{
    let kangxiRadical = kangxiRadicals[index - 1];
    let name;
    let radical;
    if (marker)
    {
        for (let cjk of kangxiRadical.cjk)
        {
            if (cjk.marker && (cjk.marker === marker))
            {
                name = cjk.name;
                radical = cjk.radical || cjk.unified;
                break;
            }
        }
    }
    else
    {
        radical = kangxiRadical.radical;
        name = kangxiRadical.name;
    }
    return (verbose ? "Radical " : "") + `${index} ${radical} (${name})`;
};
//
function fromStrokes (strokes, verbose)
{
    return `${strokes}`+ (verbose ? " Stroke" + `${strokes > 1 ? "s": ""}` : ``);
};
//
function fromRSValue (rsValue, verbose)
{
    let [ index, residual ] = rsValue.split (".");
    let marker = index.match (/('{0,3})$/)[1];  // or /('*)$/
    let result =
    [
        `${fromRadical (parseInt (index), marker, verbose)}`,
        `${fromStrokes (parseInt (residual), verbose)}`
    ];
    return result;
};
//
function fromRadicalStrokes (strokes, verbose)
{
    return `${strokes}-Stroke` + (verbose ? " Radicals" : "");
};
//
module.exports =
{
    fromRadical,
    fromStrokes,
    fromRSValue,
    fromRadicalStrokes
};
//
