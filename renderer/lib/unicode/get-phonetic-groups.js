//
const unicode = require ('./unicode.js');
let { codePoints } = require ('./parsed-unihan-data.js');
//
let phoneticGroups = { };
for (let codePoint in codePoints)
{
    let properties = codePoints[codePoint];
    for (let property in properties)
    {
        if (property === "kPhonetic")
        {
            let values = properties[property];
            if (!Array.isArray (values))
            {
                values = [ values ];
            }
            for (let value of values)
            {
                let found = value.match (/(\d+[A-D]?)(\*?)/);
                if (found)
                {
                    let [ , number, marker ] = found;
                    if (!(number in phoneticGroups))
                    {
                        phoneticGroups[number] = [ ];
                    }
                    phoneticGroups[number].push (unicode.codePointsToCharacters (codePoint) + (marker || ""));
                }
            }
        }
    }
}
//
module.exports = phoneticGroups;
//