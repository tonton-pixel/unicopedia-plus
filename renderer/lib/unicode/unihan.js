//
const regexp = require ('./regexp.js');
const unicode = require ('./unicode.js');
const { codePoints } = require ('./parsed-unihan-data.js');
const { fromRSValue } = require ('./get-rs-strings.js');
const getCompatibilitySource = require ('./get-cjk-compatibility-source.js');
//
function getTotalStrokes (character)
{
    let totalStrokes = codePoints[unicode.characterToCodePoint (character)].kTotalStrokes;
    if (Array.isArray (totalStrokes)) totalStrokes = totalStrokes[0];
    return Number (totalStrokes);
}
//
function getRSValue (character)
{
    let rsValue = codePoints[unicode.characterToCodePoint (character)].kRSUnicode;
    if (Array.isArray (rsValue)) rsValue = rsValue[0];
    return rsValue;
}
//
function getRSCollationKey (character)
{
    let [ radical, residual ] = getRSValue (character).split (".");
    let [ , index, marker ] = radical.match (/(\d+)('*)$/);
    let simplified = marker.length;
    return (parseInt (index) << 16) + (simplified << 8) + (parseInt (residual) << 0);
}
//
function getTooltip (character)
{
    let data = unicode.getCharacterBasicData (character);
    let set = "Full Unihan";
    let properties = codePoints[data.codePoint];
    if ("kIICore" in properties)
    {
        set = "IICore";
    }
    else if ("kUnihanCore2020" in properties)
    {
        set = "Unihan Core (2020)";
    }
    let isCompatibilityIdeograph = isCompatibility (character);
    let status = isCompatibilityIdeograph ? "Compatibility Ideograph" : "Unified Ideograph";
    let lines =
    [
        `Code Point: ${data.codePoint}`,
        `Block: ${data.blockName}`,
        `Age: Unicode ${data.age} (${data.ageDate})`,
        `Set: ${set}`,
        `Status: ${status}`
    ];
    if (isCompatibilityIdeograph)
    {
        lines.push (`Source: ${getCompatibilitySource (character)}`);
        lines.push (`Standardized Variant: ${data.standardizedVariation}`);
    }
    return lines.join ("\n");
}
//
function getExpandedTooltip (character)
{
    let totalStrokes = getTotalStrokes (character);
    let [ radical, strokes ] = fromRSValue (getRSValue (character));
    return getTooltip (character) + `\n----------------\nTotal Stroke Count: ${totalStrokes}\nRadical-Strokes: ${radical + " + " + strokes}`;
}
//
const unihanPattern = '^(?:(?=\\p{Script=Han})(?=\\p{Other_Letter}).)$';
const unihanRegex = regexp.build (unihanPattern, { useRegex: true });
//
function isUnihan (character)
{
    return unihanRegex.test (character);
}
//
const unifiedPattern = '^\\p{Unified_Ideograph}$';
const unifiedRegex = regexp.build (unifiedPattern, { useRegex: true });
//
function isUnified (character)
{
    return unifiedRegex.test (character);
}
//
function isCompatibility (character)
{
    return isUnihan (character) && (!isUnified (character));
}
//
// Cannot use \p{Variation_Selector} since it also includes Mongolian Free Variation Selectors
const unihanVariationPattern = '^\\p{Unified_Ideograph}[\\u{FE00}-\\u{FE0F}\\u{E0100}-\\u{E01EF}]$';
const unihanVariationRegex = regexp.build (unihanVariationPattern, { useRegex: true });

function isUnihanVariation (string)
{
    return unihanVariationRegex.test (string);
}
//
const radicalPattern = '^\\p{Radical}$';
const radicalRegex = regexp.build (radicalPattern, { useRegex: true });
//
function isRadical (character)
{
    return radicalRegex.test (character);
}
//
function validateUnifiedInput (inputString)
{
    let character = unicode.validateUnicodeInput (inputString);
    if (!isUnified (character))
    {
        character = "";
    }
    return character;
}
//
function validateUnihanInput (inputString)
{
    let character = unicode.validateUnicodeInput (inputString);
    if (!isUnihan (character))
    {
        character = "";
    }
    return character;
}
//
function validateUnihanOrRadicalInput (inputString)
{
    let character = unicode.validateUnicodeInput (inputString);
    if (!(isUnihan (character) || isRadical (character)))
    {
        character = "";
    }
    return character;
}
//
module.exports =
{
    getTotalStrokes,
    getRSCollationKey,
    getTooltip,
    getExpandedTooltip,
    isUnihan,
    isUnified,
    isCompatibility,
    isUnihanVariation,
    isRadical,
    validateUnifiedInput,
    validateUnihanInput,
    validateUnihanOrRadicalInput
};
//
