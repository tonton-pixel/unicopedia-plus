//
const regexp = require ('./regexp.js');
const unicode = require ('./unicode.js');
const { codePoints, groups } = require ('./parsed-unikemet-data.js');
//
function getTooltip (character)
{
    let data = unicode.getCharacterBasicData (character);
    let set = "Full Unikemet";
    let properties = codePoints[data.codePoint];
    if (properties["kEH_Core"] === "C")
    {
        set = "Core Unikemet";
    }
    let catalogValue = properties['kEH_Cat'];
    let [ groupCode, subgroupCode ] = catalogValue.split ("-");
    let group = groups[groupCode].label;
    let subgroup = groups[groupCode].subgroups[subgroupCode];
    let lines =
    [
        `Code Point: ${data.codePoint}`,
        `Block: ${data.blockName}`,
        `Age: Unicode ${data.age} (${data.ageDate})`,
        `Set: ${set}`,
        `Group: ${groupCode}. ${group}`,
        `Subroup: ${groupCode}${subgroupCode}. ${subgroup}`
    ];
    return lines.join ("\n");
}
//
const unikemetPattern = '^[\u{13000}-\u{1342F}\u{13460}-\u{143FA}]$';
const unikemetRegex = regexp.build (unikemetPattern, { useRegex: true });
//
function isUnikemet (character)
{
    return unikemetRegex.test (character);
}
//
function validateUnikemetInput (inputString)
{
    let character = unicode.validateUnicodeInput (inputString);
    if (!isUnikemet (character))
    {
        character = "";
    }
    return character;
}
//
const unikemetVariationPattern = '^[\u{13000}-\u{1342F}\u{13460}-\u{143FA}][\\u{FE00}-\\u{FE0F}]$';
const unikemetVariationRegex = regexp.build (unikemetVariationPattern, { useRegex: true });

function isUnikemetVariation (string)
{
    return unikemetVariationRegex.test (string);
}
//
const ivdRegex = /^<(?:[Uu]\+?)?([0-9a-fA-F]{4,8})(?:,\s*|\s+)(?:[Uu]\+?)?([0-9a-fA-F]{4,8})>$/u;
const glyphWikiRegex = /^(?:[Uu]\+?)?([0-9a-fA-F]{4,8})-(?:[Uu]\+?)?([0-9a-fA-F]{4,8})$/u;
const codePointsRegex = /^(?:[Uu]\+?)?([0-9a-fA-F]{4,8})(\s+(?:[Uu]\+?)?([0-9a-fA-F]{4,8}))?$/u;
//
function validateUnikemetSequenceInput (inputString)
{
    let sequence = "";
    inputString = inputString.trim ();
    if (isUnikemetVariation (inputString) ||isUnikemet (inputString))
    {
        sequence = inputString;
    }
    else
    {
        let match = inputString.match (ivdRegex) || inputString.match (glyphWikiRegex) || inputString.match (codePointsRegex);
        if (match)
        {
            inputString = unicode.codePointsToCharacters (inputString);
            if (isUnikemetVariation (inputString) ||isUnikemet (inputString))
            {
                sequence = inputString;
            }
        }
    }
    return sequence;
}
//
module.exports =
{
    getTooltip,
    isUnikemet,
    validateUnikemetInput,
    isUnikemetVariation,
    validateUnikemetSequenceInput
};
//
