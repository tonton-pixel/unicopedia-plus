//
const fs = require ('fs');
const path = require ('path');
//
// https://www.unicode.org/reports/tr51/
//
// Copy of https://www.unicode.org/Public/emoji/16.0/emoji-test.txt
//
function getEmojiTestPatterns ()
{
    let result = { };
    let emojiList = { };
    let lines = fs.readFileSync (path.join (__dirname, 'emoji', 'emoji-test.txt'), { encoding: 'utf8' }).split (/\r?\n/);
    for (let line of lines)
    {
        if (line && (line[0] !== '#'))
        {
            let hashOffset = line.indexOf ('#');
            let data = line.substring (0, hashOffset);
            let fields = data.split (';');
            let codePoints = fields[0].trim ().split (' ');
            let status = fields[1].trim ();
            let emojiString = "";
            for (let codePoint of codePoints)
            {
                emojiString += String.fromCodePoint (parseInt (codePoint, 16));
            }
            emojiList[emojiString] = status;
        }
    }
    //
    let emojiKeys = Object.keys (emojiList).sort ().reverse ();
    //
    let all = [ ];
    for (let key of emojiKeys)
    {
        all.push (key.replace ("*", "\\*")); // Hack!
    }
    result["Emoji_Test_All"] = '(?:' + all.join ("|") + ')';
    //
    let component = [ ];
    for (let key of emojiKeys)
    {
        if (emojiList[key] === "component")
        {
            component.push (key.replace ("*", "\\*"));  // Hack!
        }
    }
    result["Emoji_Test_Component"] = '(?:' + component.join ("|") + ')';
    //
    let keyboard = [ ];
    for (let key of emojiKeys)
    {
        if (emojiList[key] === "fully-qualified")
        {
            keyboard.push (key.replace ("*", "\\*")); // Hack!
        }
    }
    result["Emoji_Test_Keyboard"] = '(?:' + keyboard.join ("|") + ')';
    //
    let display = [ ];
    for (let key of emojiKeys)
    {
        if ((emojiList[key] === "minimally-qualified") || (emojiList[key] === "unqualified"))
        {
            display.push (key.replace ("*", "\\*")); // Hack!
        }
    }
    result["Emoji_Test_Display"] = '(?:' + display.join ("|") + ')';
    //
    return result;
}
//
module.exports = getEmojiTestPatterns ();
//